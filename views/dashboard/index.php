<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle ">
                <span class="pagetitle__title">Dashboard</span>
                <div class="btn1">
                    <a href="<?php echo Yii::$app->urlManager->createUrl('') ?>" class="pagetitle__button">Fund Campaign</a>
                </div>
                <div class="btn2">
                    <a href="<?php echo Yii::$app->urlManager->createUrl('') ?>" class="pagetitle__button">Create A Campaign</a>
                </div>
                <div class="btn2">
                    <a href="<?php echo Yii::$app->urlManager->createUrl('') ?>" class="pagetitle__button">Top Up Account</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container data-card-list">
    <?php if (isset($this->msg)) { ?>
        <div class="alert alert-success">
            <?php echo $this->msg ?>
        </div>
    <?php } ?>
    <?php if (isset($this->err)) { ?>
        <div class="alert alert-warning">
            <?php echo $this->err ?>
        </div>
    <?php } ?>

 

    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list" style="border-radius: 15px;">
                <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/gfs-icons-campaign.png">
                <span class="data-card-list__title"><?php echo $my_activecampaignCount?></span>
                <span class="data-card-list__info">My campaign</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list" style="border-radius: 15px;">
                <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/gfs-icons-meal.png">
                <span class="data-card-list__title"><?php echo $mealserved ?></span>
                <span class="data-card-list__info">Meal served</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list" style="border-radius: 15px;">
                <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/gfs-icons-meal.png">
                <span class="data-card-list__title"><?php echo  $mealunserved ?></span>
                <span class="data-card-list__info">Meals unserved</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list" style="border-radius: 15px;">
                <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/gfs-icons-stamp.png">
                <span class="data-card-list__title"><?php echo $mybalance ?></span>
                <span class="data-card-list__info">Available stamps</span>
            </div>
        </div>
        <div class="col-sm-2 col-xs-12">
       <!--     <div class="data-card-list__list">
                <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/gfs-icons-campaign.png">
                <span class="data-card-list__title"><?php echo (isset($mydonations) && isset($mydonations->object)) ? number_format($mydonations->object) : '0' ?></span>
                <span class="data-card-list__info">Total donation</span>
            </div>
        </div>-->
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="tab-menu">
                <span class="tab-menu__item">
                    <a href="#" class="active">My Campaigns</a>
                </span>
                <span class="tab-menu__item">
                    <a href="<?php echo Yii::$app->urlManager->createUrl('dashboard/my-funded-campaigns') ?>">Funded Campaigns</a>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="container block-list">
    <div class="row">
        <div class="col-xs-12">
                <?php
                foreach ($data->getModels() as $cd) { ?>
                    <a href="<?php echo Yii::$app->urlManager->createUrl('donor/viewCampaign', array('token' => $cd->token) ) ?>" class="block-list__list">
                        <?php if ($cd->is_active == 1) { ?>
                            <div class="block-list__left-holder">
                                <i class="fa fa-play"></i>
                            </div>
                        <?php } else { ?>
                            <div class="block-list__left-holder">
                                <i class="fa fa-stop"></i>
                            </div>   
                        <?php } ?>
                        <div class="block-list__right-holder">
                            <span class="main">Donation: <?php echo $cd->volume ?>, Used: Used Food Stamps</span>
                            <div class="progress-bar-green">
                                <span class="progress-bar-filler" style="width: <?php echo ($cd['volume'] == 0 ? 0 : ((($cd['used'] * 100) / $cd['volume']) )) ?>%;"></span>
                            </div>
                        </div> 
                        <div class="block-list__main-holder">
                            <div class="block-list__title"><?php echo $cd->title ?></div>
                            <div class="block-list__info">Created: <?php echo $cd->created_time ?> | Location: <?php //echo $cd->state_name ?></div>
                        </div>
                    </a>
                <?php } ?>
                   <?= LinkPager::widget(['pagination' => $pagination,]); ?>
        </div>
    </div>
</div>


