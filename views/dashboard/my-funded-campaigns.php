<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <span class="pagetitle__title">Funded Campaigns</span>
                <a href="<?php echo Yii::app()->createUrl('donor/createCampaign') ?>" class="pagetitle__button btn btn-info">Create A Campaign</a>
            </div>
        </div>
    </div>
</div>

<div class="container data-card-list">
    <?php if (isset($this->msg)) { ?>
        <div class="alert alert-success">
            <?php echo $this->msg ?>
        </div>
    <?php } ?>
    <?php if (isset($this->err)) { ?>
        <div class="alert alert-warning">
            <?php echo $this->err ?>
        </div>
    <?php } ?>
    


    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list">
                <span class="data-card-list__title"><?php echo $myFundedCampaignsCount ?></span>
                <span class="data-card-list__info">My Funded Campaign</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list">
                <span class="data-card-list__title"><?php echo $mealserved ?></span>
                <span class="data-card-list__info">Meal Served</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list">
                <span class="data-card-list__title"><?php echo $mealunserved ?></span>
                <span class="data-card-list__info">Meal Unserved</span>
            </div>
        </div>
        <div class="col-sm-3 col-xs-12">
            <div class="data-card-list__list">
                <span class="data-card-list__title"><?php echo $mydonations ?></span>
                <span class="data-card-list__info">Stamps Donated</span>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="tab-menu">
                <span class="tab-menu__item">
                    <a href="<?php echo $this->createUrl('donor/dashboard') ?>">My Campaigns</a>
                </span>
                <span class="tab-menu__item">
                    <a  class="active" href="<?php echo $this->createUrl('donor/myFundedCampaigns') ?>">Funded Campaigns</a>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="container block-list">

    <div class="row">
        <div class="col-xs-12">
            
                <?php
                foreach ( $myFundedCampaigns as $myactivecampaign) { ?>
                    <a href="<?php echo $this->createUrl('donor/viewOtherCampaign', array('token' => $myactivecampaign['token'])) ?>" class="block-list__list">
                        <?php if ($myactivecampaign['is_active'] == 1) { ?>
                            <div class="block-list__left-holder">
                                <i class="fa fa-play"></i>
                            </div>
                        <?php } else { ?>
                            <div class="block-list__left-holder">
                                <i class="fa fa-stop"></i>
                            </div>   
                        <?php } ?>
                        <div class="block-list__right-holder">
                            <span class="main">Donation: <?php echo $myactivecampaign['volume'] ?>, Used: <?php echo $myactivecampaign['used']?> Food Stamps</span>
                            <div class="progress-bar-green">
                                <span class="progress-bar-filler" style="width: <?php echo ($myactivecampaign['volume'] == 0 ? 0 : ((($myactivecampaign['used'] * 100) / $myactivecampaign['volume']) )) ?>%;"></span>
                            </div>
                        </div>
                        <div class="block-list__main-holder">
                            <div class="block-list__title"><?php echo $myactivecampaign['title'] ?></div>
                            <div class="block-list__info">Created: <?php echo $myactivecampaign['created_time'] ?> 
                                | Location: <?php echo $myactivecampaign['state_name'] ?></div>
                        </div>
                    </a>

                <?php } ?>

              

        </div>
    </div>
</div>