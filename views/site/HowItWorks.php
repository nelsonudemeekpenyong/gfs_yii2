
    
    <div class="container">
        <div class="wrapper">
            <div class="row row-offcanvas row-offcanvas-left gfs-inner">
        <!-- sidebar -->
        <div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
             <ul class="nav" id="menu">
                <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>" ><span class="collapse in hidden-xs">About</span></a></li>
                <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>" class="active"> <span class="collapse in hidden-xs">How it Works</span></a></li>
                <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/stakeholders') ?>"> <span class="collapse in hidden-xs">Stakeholders</span></a></li>
                <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/board') ?>"><span class="collapse in hidden-xs">Board</span></a></li>
           
           </ul>
        </div>
        <!-- /sidebar -->

        <!-- main right col -->
        <div class="column col-sm-9 col-xs-12" id="main">
            <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a>
           <h1 class="big-title" id="about" >How It Works</h1>
            <hr/>
            <article class="regular-text-left">
             
                <p> The feeding program works in five simple steps:</p>
            <ul>
                <li>Donor sponsors food stamps</li>
                <li>Food stamps are distributed to beneficiaries</li>
            <li>Beneficiaries redeem at participating food canteens</li>
            <li>Food vendor receive and validate the food stamps before serving food</li>
            <li>At close of business, the bank credits the food vendors daily for food stamps redeemed.</li>
            </ul>
                
                <p class="regular-text-left">On the GFS Platform, Donors may determine and restrict the regions for which their food stamps are distributed and can also monitor the redemption of the food stamps in real-time.
                Under this program there is an assurance that the beneficiary will consume a hot meal as each food stamp is tracked by geographical location and by the redeeming food vendor.
                The food vendors may also track their redemptions at any time of the day using their phones.</p>
            </article>
        </div>
        <!-- /main -->
    </div>
</div>
    </div>
    
  
    