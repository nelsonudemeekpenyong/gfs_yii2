<div class="container form-page">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 ">
                <h1 class="main-title text-center">Register to be a Donor</h1>
                <form action="<?php echo Yii::app()->createUrl('donor/register') ?>" class="form-block form-block-shadow" method="post">
                    <?php if(isset($err)){ ?>
                    <div class="alert alert-warning">
                        <?php foreach($err as $e){ ?>
                        <p> <?php echo $e ?></p>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <label>
                        <span>First Name </span>
                        <input type="text" name="firstname" class="form__input" placeholder="Enter Your First Name" required="required">
                    </label>
                    <label>
                        <span>Last Name</span>
                        <input type="text" name="lastname" class="form__input" placeholder="Enter Your Last Name" required="required">
                    </label>
                    <label>
                        <span>Email</span>
                        <input type="email" name="email" class="form__input" placeholder="Enter Your Email" required="required">
                    </label>
                    <label>
                        <span>Password</span>
                        <input type="password" name="password" class="form__input" placeholder="Enter Your Password">
                    </label>
                    <label>
                        <span>Confirm Password</span>
                        <input type="password" name="confirm_password" class="form__input" placeholder="Confirm Password">
                    </label>
                    <label>
                        <span>Account Type</span>
                        <select name="account_type" class="form__select" required="required">
                            <option value="">Select One</option>
                            <option>Individual</option>
                            <option>Corporate</option>
                        </select>
                    </label>
                    <label>
                        <span>Address</span>
                        <input type="text" name="address" class="form__input" placeholder="Enter Your Address" required="required">
                    </label>
                    <div class="row ">
                        <label class="col-md-6">
                            <span>State</span>
                            <select name="state" class="form__select" id="donor-state" required="required">
                                <option value="">--Select State--</option>
                                <option>Lagos</option>
                                <option>Abuja</option>
                                <option>Abia</option>
                                <option>Adamawa</option>
                                <option>Akwa-Ibom</option>
                                <option>Anambra</option>
                                <option>Bauchi</option>
                                <option>Bayelsa</option>
                                <option>Benue</option>
                                <option>Borno</option>
                                <option>Cross-River</option>
                                <option>Delta</option>
                                <option>Ebonyi</option>
                                <option>Edo</option>
                                <option>Ekiti</option>
                                <option>Enugu</option>
                                <option>Gombe</option>
                                <option>Imo</option>
                                <option>Jigawa</option>
                                <option>Kaduna</option>
                                <option>Kano</option>
                                <option>Katsina</option>
                                <option>Kebbi</option>
                                <option>Kogi</option>
                                <option>Kwara</option>
                                <option>Nasarawa</option>
                                <option>Niger</option>
                                <option>Ogun</option>
                                <option>Ondo</option>
                                <option>Osun</option>
                                <option>Oyo</option>
                                <option>Plateau</option>
                                <option>Rivers</option>
                                <option>Sokoto</option>
                                <option>Taraba</option>
                                <option>Yobe</option>
                                <option>Zamfara</option>
                            </select>
                        </label>
                        <label class="col-md-6">
                            <span>LGA</span>
                            <select name="lga" class="form__select" id="donor-lga" required="required">
                                <option>--Select LGA--</option>
                            </select>
                        </label>

                    </div>
                    <label>
                        <input type="submit" value="Register" class="button-green inline button-large">
                    </label>
                    <div class="text-center "><strong><a href="<?php echo Yii::app()->createUrl('site/login') ?>" class="blue-text">Already have an Account? Login</a></strong></div>
                </form>
            </div>
            
            <div class="col-md-6 ">
                
                <h1 class="main-title text-center">Register to be a Partner</h1>
                
                <form action="<?php echo Yii::app()->createUrl('partner/register') ?>" class="form-block form-block-shadow" method="post">
                    <?php if(isset($err)){ ?>
                    <div class="alert alert-warning">
                        <?php foreach($err as $e){ ?>
                        <p> <?php echo $e ?></p>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <label>
                        <span>First Name </span>
                        <input type="text" name="firstname" class="form__input" placeholder="Enter Your First Name" required="required">
                    </label>
                    <label>
                        <span>Last Name</span>
                        <input type="text" name="lastname" class="form__input" placeholder="Enter Your Last Name" required="required">
                    </label>
                    <label>
                        <span>Organization</span>
                        <input type="text" name="organisationName" class="form__input" placeholder="Enter Your Organization Name" required="required">
                    </label>
                    <label>
                        <span>Email Address</span>
                        <input type="email" name="e_mail" class="form__input" placeholder="Enter Your Email" required="required">
                    </label>
                    <label>
                        <span>Category</span>
                        <select name="categoryToken" class="form__select" id="categoryToken" required="required">
                            <option value="">--Select One--</option>
                            <?php if(isset($category) && isset($category->object)){ ?>
                                <?php foreach($category->object as $cat){ ?>
                                    <option value="<?php echo $cat->token ?>"><?php echo $cat->category ?></option>
                                <?php } ?>
                            <?php } ?>

                        </select>
                    </label>
                    <label>
                        <span>Description</span>
                        <textarea class="form__textarea" name="description" placeholder="Describe your Organization" required="required"></textarea>
                    </label>
                    <label>
                        <span>Operating State</span>
                        <select name="organisationState" class="form__select" id="organisationState" required="required">
                            <option value="">--Select Operating States--</option>
                            <option>Lagos</option>
                            <option>Abuja</option>
                            <option>Abia</option>
                            <option>Adamawa</option>
                            <option>Akwa-Ibom</option>
                            <option>Anambra</option>
                            <option>Bauchi</option>
                            <option>Bayelsa</option>
                            <option>Benue</option>
                            <option>Borno</option>
                            <option>Cross-River</option>
                            <option>Delta</option>
                            <option>Ebonyi</option>
                            <option>Edo</option>
                            <option>Ekiti</option>
                            <option>Enugu</option>
                            <option>Gombe</option>
                            <option>Imo</option>
                            <option>Jigawa</option>
                            <option>Kaduna</option>
                            <option>Kano</option>
                            <option>Katsina</option>
                            <option>Kebbi</option>
                            <option>Kogi</option>
                            <option>Kwara</option>
                            <option>Nasarawa</option>
                            <option>Niger</option>
                            <option>Ogun</option>
                            <option>Ondo</option>
                            <option>Osun</option>
                            <option>Oyo</option>
                            <option>Plateau</option>
                            <option>Rivers</option>
                            <option>Sokoto</option>
                            <option>Taraba</option>
                            <option>Yobe</option>
                            <option>Zamfara</option>
                        </select>
                    </label>
                    <label>
                        <span>Contact Number</span>
                        <input type="number" name="number" class="form__input" placeholder="Enter Contact Number" required="required">
                    </label>
                    <label>
                        <span>Contact Address</span>
                        <input type="text" name="address" class="form__input" placeholder="Enter Contact Address" required="required">
                    </label>
                    <label>
                        <span>Country</span>
                        <select name="countryToken" class="form__select" id="countryToken" required="required">
                            <option value="">--Select Country--</option>
                            <?php if(isset($country) && isset($country->object)){ ?>
                                <?php foreach($country->object as $countr){ ?>
                                    <option value="<?php echo $countr->token ?>"><?php echo $countr->name ?></option>
                                <?php } ?>
                            <?php } ?>

                        </select>
                    </label>
                    <div class="row ">
                        <label class="col-md-6">
                            <span>State</span>
                            <select name="state" class="form__select" id="partner-state" required="required">
                                <option value="">--Select State--</option>
                                <option>Lagos</option>
                                <option>Abuja</option>
                                <option>Abia</option>
                                <option>Adamawa</option>
                                <option>Akwa-Ibom</option>
                                <option>Anambra</option>
                                <option>Bauchi</option>
                                <option>Bayelsa</option>
                                <option>Benue</option>
                                <option>Borno</option>
                                <option>Cross-River</option>
                                <option>Delta</option>
                                <option>Ebonyi</option>
                                <option>Edo</option>
                                <option>Ekiti</option>
                                <option>Enugu</option>
                                <option>Gombe</option>
                                <option>Imo</option>
                                <option>Jigawa</option>
                                <option>Kaduna</option>
                                <option>Kano</option>
                                <option>Katsina</option>
                                <option>Kebbi</option>
                                <option>Kogi</option>
                                <option>Kwara</option>
                                <option>Nasarawa</option>
                                <option>Niger</option>
                                <option>Ogun</option>
                                <option>Ondo</option>
                                <option>Osun</option>
                                <option>Oyo</option>
                                <option>Plateau</option>
                                <option>Rivers</option>
                                <option>Sokoto</option>
                                <option>Taraba</option>
                                <option>Yobe</option>
                                <option>Zamfara</option>
                            </select>
                        </label>
                        <label class="col-md-6">
                            <span>LGA</span>
                            <select name="lga" class="form__select" id="partner-lga" required="required">
                                <option>--Select LGA--</option>
                            </select>
                        </label>
                    </div>
                    <label>
                        <span>Password</span>
                        <input type="password" name="password" class="form__input" placeholder="Enter Your Password">
                    </label>
                    <label>
                        <span>Confirm Password</span>
                        <input type="password" name="confirm_password" class="form__input" placeholder="Confirm Password">
                    </label>

                    <label>
                        <input type="submit" value="Register" class="button-green inline button-large">
                    </label>
                    <div class="text-center "><strong><a href="<?php echo Yii::app()->createUrl('site/login') ?>" class="blue-text">Already have an Account? Login</a></strong></div>
                </form>
            </div>
            
            
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>



<div style="display: none;" id="s0">
    <option>--Select LGA--</option>
</div>

<div style="display: none;" id="Lagos">
    <option>Agege</option>
    <option>Ajeromi-Ifelodun</option>
    <option>Alimosho</option>
    <option>Amuwo-Odofin</option>
    <option>Apapa</option>
    <option>Badagry</option>
    <option>Epe</option>
    <option>Eti-Osa</option>
    <option>Ibeju/Lekki</option>
    <option>Ifako-Ijaye</option>
    <option>Ikeja</option>
    <option>Ikorodu</option>
    <option>Kosofe</option>
    <option>Lagos Island</option>
    <option>Lagos Mainland</option>
    <option>Mushin</option>
    <option>Ojo</option>
    <option>Oshodi-Isolo</option>
    <option>Shomolu</option>
    <option>Surulere</option>
</div>

<div style="display: none;" id="Abuja">
    <option>Gwagwalada</option>
    <option>Kuje</option>
    <option>Abaji</option>
    <option>Abuja Municipal</option>
    <option>Bwari</option>
    <option>Kwali</option>
</div>

<div style="display: none;" id="Abia">
    <option>Aba North</option>
    <option>Aba South</option>
    <option>Arochukwu</option>
    <option>Bende</option>
    <option>Ikwuano</option>
    <option>Isiala Ngwa North</option>
    <option>Isiala Ngwa South</option>
    <option>Isiukwuato</option>
    <option>Obi Ngwa</option>
    <option>Ohafia</option>
    <option>Osisioma Ngwa</option>
    <option>Ugwunagbo</option>
    <option>Ukwa East</option>
    <option>Ukwa West</option>
    <option>Umuahia North</option>
    <option>Umuahia South</option>
    <option>Umunneochi</option>
</div>

<div style="display: none;" id="Adamawa">
    <option>Demsa</option>
    <option>Fufore</option>
    <option>Ganaye</option>
    <option>Gireri</option>
    <option>Gombi</option>
    <option>Guyuk</option>
    <option>Hong</option>
    <option>Jada</option>
    <option>Lamurde</option>
    <option>Madagali</option>
    <option>Maiha</option>
    <option>Mayo-Belwa</option>
    <option>Michika</option>
    <option>Mubi North</option>
    <option>Mubi South</option>
    <option>Numan</option>
    <option>Shelleng</option>
    <option>Song</option>
    <option>Toungo</option>
</div>

<div style="display: none;" id="Akwa-Ibom">
    <option>Abak</option>
    <option>Eastern Obolo</option>
    <option>Eket</option>
    <option>Esit Eket</option>
    <option>Essien Udim</option>
    <option>Etim Ekpo</option>
    <option>Etinan</option>
    <option>Ibeno</option>
    <option>Ibesikpo Asutan</option>
    <option>Ibiono Ibom</option>
    <option>Ika</option>
    <option>Ikono</option>
    <option>Ikot Abasi</option>
    <option>Ikot Ekpene</option>
    <option>Ini</option>
    <option>Itu</option>
    <option>Mbo</option>
    <option>Mkpat Enin</option>
    <option>Nsit Atai</option>
    <option>Nsit Ibom</option>
    <option>Nsit Ubium</option>
    <option>Obot Akara</option>
    <option>Okobo</option>
    <option>Onna</option>
    <option>Oron</option>
    <option>Oruk Anam</option>
    <option>Udung Uko</option>
    <option>Ukanafun</option>
    <option>Uruan</option>
    <option>Urue-Offong/Oruko</option>
    <option>Uyo</option>
</div>

<div style="display: none;" id="Anambra">
    <option>Anambra East</option>
    <option>Anambra West</option>
    <option>Anaocha</option>
    <option>Awka North</option>
    <option>Ayamelum</option>
    <option>Dunukofia</option>
    <option>Ekwusigo</option>
    <option>Idemili North</option>
    <option>Idemili south</option>
    <option>Ihiala</option>
    <option>Njikoka</option>
    <option>Nnewi North</option>
    <option>Nnewi South</option>
    <option>Ogbaru</option>
    <option>Onitsha North</option>
    <option>Onitsha South</option>
    <option>Orumba North</option>
    <option>Orumba South</option>
    <option>Oyi</option>
</div>

<div style="display: none;" id="Bauchi">
    <option>Bauchi</option>
    <option>Bogoro</option>
    <option>Damban</option>
    <option>Darazo</option>
    <option>Dass</option>
    <option>Ganjuwa</option>
    <option>Giade</option>
    <option>Itas/Gadau</option>
    <option>Jama’are</option>
    <option>Katagum</option>
    <option>Kirfi</option>
    <option>Misau</option>
    <option>Ningi</option>
    <option>Shira</option>
    <option>Tafawa-Balewa</option>
    <option>Toro</option>
    <option>Warji</option>
    <option>Zaki</option>
</div>

<div style="display: none;" id="Bayelsa">
    <option>Brass</option>
    <option>Ekeremor</option>
    <option>Kolokuma/Opokuma</option>
    <option>Nembe</option>
    <option>Ogbia</option>
    <option>Sagbama</option>
    <option>Southern Jaw</option>
    <option>Yenegoa</option>
</div>

<div style="display: none;" id="Benue">
    <option>Ado</option>
    <option>Agatu</option>
    <option>Apa</option>
    <option>Buruku</option>
    <option>Gboko</option>
    <option>Guma</option>
    <option>Gwer East</option>
    <option>Gwer West</option>
    <option>Katsina-Ala</option>
    <option>Konshisha</option>
    <option>Kwande</option>
    <option>Logo</option>
    <option>Makurdi</option>
    <option>Obi</option>
    <option>Ogbadibo</option>
    <option>Oju</option>
    <option>Okpokwu</option>
    <option>Ohimini</option>
    <option>Oturkpo</option>
    <option>Tarka</option>
    <option>Ukum</option>
    <option>Ushongo</option>
    <option>Vandeikya</option>
</div>

<div style="display: none;" id="Borno">
    <option>Askira/Uba</option>
    <option>Bama</option>
    <option>Bayo</option>
    <option>Biu</option>
    <option>Chibok</option>
    <option>Damboa</option>
    <option>Dikwa</option>
    <option>Gubio</option>
    <option>Guzamala</option>
    <option>Gwoza</option>
    <option>Hawul</option>
    <option>Jere</option>
    <option>Kaga</option>
    <option>Kala/Balge</option>
    <option>Konduga</option>
    <option>Kukawa</option>
    <option>Kwaya Kusar</option>
    <option>Mafav</option>
    <option>Magumeri</option>
    <option>Maiduguri</option>
    <option>Marte</option>
    <option>Mobbar</option>
    <option>Monguno</option>
    <option>Ngala</option>
    <option>Nganzai</option>
    <option>Shani</option>
</div>

<div style="display: none;" id="Cross-River">
    <option>Odukpani</option>
    <option>Akamkpa</option>
    <option>Biasev</option>
    <option>Abi</option>
    <option>Ikom</option>
    <option>Yarkur</option>
    <option>Odubra</option>
    <option>Boki</option>
    <option>Ogoja</option>
    <option>Yala</option>
    <option>Obanliku</option>
    <option>Obudu</option>
    <option>Calabar South</option>
    <option>Etung</option>
    <option>Bekwara</option>
    <option>Bakassi</option>
    <option>Calabar Municipality</option>
</div>

<div style="display: none;" id="Delta">
    <option>Oshimili</option>
    <option>Aniocha</option>
    <option>Aniocha South</option>
    <option>Ika South</option>
    <option>Ika North-East</option>
    <option>Ndokwa West</option>
    <option>Ndokwa East</option>
    <option>Isoko south</option>
    <option>Isoko North</option>
    <option>Bomadi</option>
    <option>Burutu</option>
    <option>Ughelli South</option>
    <option>Ughelli North</option>
    <option>Ethiope West</option>
    <option>Ethiope East</option>
    <option>Sapele</option>
    <option>Okpe</option>
    <option>Warri North</option>
    <option>Warri South</option>
    <option>Uvwie</option>
    <option>Udu</option>
    <option>Warri Central</option>
    <option>Ukwani</option>
    <option>Oshimili North</option>
    <option>Patani</option>
</div>

<div style="display: none;" id="Ebonyi">
    <option>Afikpo South</option>
    <option>Afikpo North</option>
    <option>Onicha</option>
    <option>Ohaozara</option>
    <option>Abakaliki</option>
    <option>Ishielu</option>
    <option>lkwo</option>
    <option>Ezza</option>
    <option>Ezza South</option>
    <option>Ohaukwu</option>
    <option>Ebonyi</option>
    <option>Ivo</option>
</div>

<div style="display: none;" id="Edo">
    <option>Esan North-East</option>
    <option>Esan Central</option>
    <option>Esan West</option>
    <option>Egor</option>
    <option>Ukpoba</option>
    <option>Central</option>
    <option>Etsako Central</option>
    <option>Igueben</option>
    <option>Oredo</option>
    <option>Ovia SouthWest</option>
    <option>Ovia South-East</option>
    <option>Orhionwon</option>
    <option>Uhunmwonde</option>
    <option>Etsako East</option>
    <option>Esan South-East</option>
</div>

<div style="display: none;" id="Ekiti">
    <option>Ado</option>
    <option>Ekiti-East</option>
    <option>Ekiti-West</option>
    <option>Emure/Ise/Orun</option>
    <option>Ekiti South-West</option>
    <option>Ikare</option>
    <option>Irepodun</option>
    <option>Ijero</option>
    <option>Ido/Osi</option>
    <option>Oye</option>
    <option>Ikole</option>
    <option>Moba</option>
    <option>Gbonyin</option>
    <option>Efon</option>
    <option>Ise/Orun</option>
    <option>Ilejemeje</option>
</div>

<div style="display: none;" id="Enugu">
    <option>Enugu South</option>
    <option>Igbo-Eze South</option>
    <option>Enugu North</option>
    <option>Nkanu</option>
    <option>Udi Agwu</option>
    <option>Oji-River</option>
    <option>Ezeagu</option>
    <option>IgboEze North</option>
    <option>Isi-Uzo</option>
    <option>Nsukka</option>
    <option>Igbo-Ekiti</option>
    <option>Uzo-Uwani</option>
    <option>Enugu East</option>
    <option>Aninri</option>
    <option>Nkanu East</option>
    <option>Udenu</option>
</div>

<div style="display: none;" id="Gombe">
    <option>Akko</option>
    <option>Balanga</option>
    <option>Billiri</option>
    <option>Dukku</option>
    <option>Kaltungo</option>
    <option>Kwami</option>
    <option>Shomgom</option>
    <option>Funakaye</option>
    <option>Gombe</option>
    <option>Nafada/Bajoga</option>
    <option>Yamaltu/Delta</option>
</div>

<div style="display: none;" id="Imo">
    <option>Aboh-Mbaise</option>
    <option>Ahiazu-Mbaise</option>
    <option>Ehime-Mbano</option>
    <option>Ezinihitte</option>
    <option>Ideato North</option>
    <option>Ideato South</option>
    <option>Ihitte/Uboma</option>
    <option>Ikeduru</option>
    <option>Isiala Mbano</option>
    <option>Isu</option>
    <option>Mbaitoli</option>
    <option>Mbaitoli</option>
    <option>Ngor-Okpala</option>
    <option>Njaba</option>
    <option>Nwangele</option>
    <option>Nkwerre</option>
    <option>Obowo</option>
    <option>Oguta</option>
    <option>Ohaji/Egbema</option>
    <option>Okigwe</option>
    <option>Orlu</option>
    <option>Orsu</option>
    <option>Oru East</option>
    <option>Oru West</option>
    <option>Owerri-Municipal</option>
    <option>Owerri North</option>
    <option>Owerri West</option>
</div>

<div style="display: none;" id="Jigawa">
    <option>Auyo</option>
    <option>Birniwa</option>
    <option>Babura</option>
    <option>Buji</option>
    <option>Birnin Kudu</option>
    <option>Dutse</option>
    <option>Gumel</option>
    <option>Gwaram</option>
    <option>Gwiwa</option>
    <option>Gagarawa</option>
    <option>Guri</option>
    <option>Garki</option>
    <option>Hadejia</option>
    <option>Jahun</option>
    <option>Kiri Kasamma</option>
    <option>Kafin Hausa</option>
    <option>Kazaure</option>
    <option>Kiyawa</option>
    <option>Kaugama</option>
    <option>Malam Madori</option>
    <option>Miga</option>
    <option>Maigatari</option>
    <option>Roni</option>
    <option>Ringim</option>
    <option>Sule Tankarkar</option>
    <option>Taura</option>
    <option>Yankwashi</option>
</div>

<div style="display: none;" id="Kaduna">
    <option>Birni-Gwari</option>
    <option>Chikun</option>
    <option>Giwa</option>
    <option>Igabi</option>
    <option>Ikara</option>
    <option>Jaba</option>
    <option>Jema’a</option>
    <option>Kachia</option>
    <option>Kaduna North</option>
    <option>Kaduna South</option>
    <option>Kagarko</option>
    <option>Kajuru</option>
    <option>Kaura</option>
    <option>Kauru</option>
    <option>Kubau</option>
    <option>Kudan</option>
    <option>Lere</option>
    <option>Makarfi</option>
    <option>Sabon-Gari</option>
    <option>Sanga</option>
    <option>Soba</option>
    <option>Zango-Kataf</option>
    <option>Zaria</option>
</div>

<div style="display: none;" id="Kano">
    <option>Ajingi</option>
    <option>Albasu</option>
    <option>Bagwai</option>
    <option>Bebeji</option>
    <option>Bichi</option>
    <option>Bunkure</option>
    <option>Dala</option>
    <option>Dambatta</option>
    <option>Dawakin Kudu</option>
    <option>Dawakin Tofa</option>
    <option>Doguwa</option>
    <option>Fagge</option>
    <option>Gabasawa</option>
    <option>Garko</option>
    <option>Garum</option>
    <option>Mallam</option>
    <option>Gaya</option>
    <option>Gezawa</option>
    <option>Gwale</option>
    <option>Gwarzo</option>
    <option>Kabo</option>
    <option>Kano Municipal</option>
    <option>Karaye</option>
    <option>Kibiya</option>
    <option>Kiru</option>
    <option>kumbotso</option>
    <option>Kunchi</option>
    <option>Kura</option>
    <option>Madobi</option>
    <option>Makoda</option>
    <option>Minjibir</option>
    <option>Nasarawa</option>
    <option>Rano</option>
    <option>Rimin Gado</option>
    <option>Rogo</option>
    <option>Shanono</option>
    <option>Sumaila</option>
    <option>Takali</option>
    <option>Tarauni</option>
    <option>Tofa</option>
    <option>Tsanyawa</option>
    <option>Tudun Wada</option>
    <option>Ungogo</option>
    <option>Warawa</option>
    <option>Wudil</option>
</div>

<div style="display: none;" id="Katsina">
    <option>Bakori</option>
    <option>Batagarawa</option>
    <option>Batsari</option>
    <option>Baure</option>
    <option>Bindawa</option>
    <option>Charanchi</option>
    <option>Dandume</option>
    <option>Danja</option>
    <option>Dan Musa</option>
    <option>Daura</option>
    <option>Dutsi</option>
    <option>Dutsin-Ma</option>
    <option>Faskari</option>
    <option>Funtua</option>
    <option>Ingawa</option>
    <option>Jibia</option>
    <option>Kafur</option>
    <option>Kaita</option>
    <option>Kankara</option>
    <option>Kankia</option>
    <option>Katsina</option>
    <option>Kurfi</option>
    <option>Kusada</option>
    <option>Mai’Adua</option>
    <option>Malumfashi</option>
    <option>Mani</option>
    <option>Mashi</option>
    <option>Matazuu</option>
    <option>Musawa</option>
    <option>Rimi</option>
    <option>Sabuwa</option>
    <option>Safana</option>
    <option>Sandamu</option>
    <option>Zango</option>
</div>

<div style="display: none;" id="Kebbi">
    <option>Aleiro</option>
    <option>Arewa-Dandi</option>
    <option>Argungu</option>
    <option>Augie</option>
    <option>Bagudo</option>
    <option>Birnin Kebbi</option>
    <option>Bunza</option>
    <option>Dandi</option>
    <option>Fakai</option>
    <option>Gwandu</option>
    <option>Jega</option>
    <option>Kalgo</option>
    <option>Koko/Besse</option>
    <option>Maiyama</option>
    <option>Ngaski</option>
    <option>Sakaba</option>
    <option>Shanga</option>
    <option>Suru</option>
    <option>Wasagu/Danko</option>
    <option>Yauri</option>
    <option>Zuru</option>
</div>

<div style="display: none;" id="Kogi">
    <option>Adavi</option>
    <option>Ajaokuta</option>
    <option>Ankpa</option>
    <option>Bassa</option>
    <option>Dekina</option>
    <option>Ibaji</option>
    <option>Idah</option>
    <option>Igalamela-Odolu</option>
    <option>Ijumu</option>
    <option>Kabba/Bunu</option>
    <option>Kogi</option>
    <option>Lokoja</option>
    <option>Mopa-Muro</option>
    <option>Ofu</option>
    <option>Ogori/Mangongo</option>
    <option>Okehi</option>
    <option>Okene</option>
    <option>Olamabolo</option>
    <option>Omala</option>
    <option>Yagba East</option>
    <option>Yagba West</option>
</div>

<div style="display: none;" id="Kwara">
    <option>Asa</option>
    <option>Baruten</option>
    <option>Edu</option>
    <option>Ekiti</option>
    <option>Ifelodun</option>
    <option>Ilorin East</option>
    <option>Ilorin West</option>
    <option>Irepodun</option>
    <option>Isin</option>
    <option>Kaiama</option>
    <option>Moro</option>
    <option>Offa</option>
    <option>Oke-Ero</option>
    <option>Oyun</option>
    <option>Pategi</option>
</div>

<div style="display: none;" id="Nasarawa">
    <option>Akwanga</option>
    <option>Awe</option>
    <option>Doma</option>
    <option>Karu</option>
    <option>Keana</option>
    <option>Keffi</option>
    <option>Kokona</option>
    <option>Lafia</option>
    <option>Nasarawa</option>
    <option>Nasarawa-Eggon</option>
    <option>Obi</option>
    <option>Toto</option>
    <option>Wamba</option>
</div>

<div style="display: none;" id="Niger">
    <option>Agaie</option>
    <option>Agwara</option>
    <option>Bida</option>
    <option>Borgu</option>
    <option>Bosso</option>
    <option>Chanchaga</option>
    <option>Edati</option>
    <option>Gbako</option>
    <option>Gurara</option>
    <option>Katcha</option>
    <option>Kontagora</option>
    <option>Lapai</option>
    <option>Lavun</option>
    <option>Magama</option>
    <option>Mariga</option>
    <option>Mashegu</option>
    <option>Mokwa</option>
    <option>Muya</option>
    <option>Pailoro</option>
    <option>Rafi</option>
    <option>Rijau</option>
    <option>Shiroro</option>
    <option>Suleja</option>
    <option>Tafa</option>
    <option>Wushishi</option>
</div>

<div style="display: none;" id="Ogun">
    <option>Abeokuta South</option>
    <option>Abeokuta North</option>
    <option>Ado-ota/Ota</option>
    <option>Ewekoro</option>
    <option>Ifo</option>
    <option>Imeko/Afon</option>
    <option>Ijebu–North East</option>
    <option>Ijebu–North</option>
    <option>Ijebu–Ode</option>
    <option>Ijebu–East</option>
    <option>Ikenne</option>
    <option>Ipokia</option>
    <option>Odogbolu</option>
    <option>Obafemi/Owode</option>
    <option>Odeda</option>
    <option>Remo North</option>
    <option>Sagamu</option>
    <option>Waterside</option>
    <option>Yewa North</option>
    <option>Yewa South</option>
</div>

<div style="display: none;" id="Ondo">
    <option>Akoko North East</option>
    <option>Akoko North West</option>
    <option>Akoko South Akure East</option>
    <option>Akoko South West</option>
    <option>Akure North</option>
    <option>Akure South</option>
    <option>Ese-Odo</option>
    <option>Idanre</option>
    <option>Ifedore</option>
    <option>Ilaje</option>
    <option>Ile-Oluji</option>
    <option>Irele</option>
    <option>Odigbo</option>
    <option>Okitipupa</option>
    <option>Ondo East</option>
    <option>Ondo West</option>
    <option>Ose</option>
    <option>Owo</option>
</div>

<div style="display: none;" id="Osun">
    <option>Aiyedade</option>
    <option>Aiyedire</option>
    <option>Atakumosa East</option>
    <option>Atakumosa West</option>
    <option>Boluwaduro</option>
    <option>Boripe</option>
    <option>Ede North</option>
    <option>Ede South</option>
    <option>Egbedore</option>
    <option>Ejigbo</option>
    <option>Ife Central</option>
    <option>Ife East</option>
    <option>Ife North</option>
    <option>Ife South</option>
    <option>Ifedayo</option>
    <option>Ifelodun</option>
    <option>Ila</option>
    <option>Ilesha East</option>
    <option>Ilesha West</option>
    <option>Irepodun</option>
    <option>Irewole</option>
    <option>Isokan</option>
    <option>Iwo</option>
    <option>Obokun</option>
    <option>Odo-Otin</option>
    <option>Ola-Oluwa</option>
    <option>Olorunda</option>
    <option>Oriade</option>
    <option>Orolu</option>
    <option>Osogbo</option>
</div>

<div style="display: none;" id="Oyo">
    <option>Afijio</option>
    <option>Akinyele</option>
    <option>Atiba</option>
    <option>Atigbo</option>
    <option>Egbeda</option>
    <option>IbadanCentral</option>
    <option>Ibadan North</option>
    <option>Ibadan North West</option>
    <option>Ibadan South East</option>
    <option>Ibadan South West</option>
    <option>Ibarapa Central</option>
    <option>Ibarapa East</option>
    <option>Ibarapa North</option>
    <option>Ido</option>
    <option>Irepo</option>
    <option>Iseyin</option>
    <option>Itesiwaju</option>
    <option>Iwajowa</option>
    <option>Kajola</option>
    <option>Lagelu Ogbomosho North</option>
    <option>Ogbmosho South</option>
    <option>Ogo Oluwa</option>
    <option>Olorunsogo</option>
    <option>Oluyole</option>
    <option>Ona-Ara</option>
    <option>Orelope</option>
    <option>Ori Ire</option>
    <option>Oyo East</option>
    <option>Oyo West</option>
    <option>Saki East</option>
    <option>Saki West</option>
    <option>Surulere</option>
</div>

<div style="display: none;" id="Plateau">
    <option>Barikin Ladi</option>
    <option>Bassa</option>
    <option>Bokkos</option>
    <option>Jos East</option>
    <option>Jos North</option>
    <option>Jos South</option>
    <option>Kanam</option>
    <option>Kanke</option>
    <option>Langtang North</option>
    <option>Langtang South</option>
    <option>Mangu</option>
    <option>Mikang</option>
    <option>Pankshin</option>
    <option>Qua’an Pan</option>
    <option>Riyom</option>
    <option>Shendam</option>
    <option>Wase</option>
</div>

<div style="display: none;" id="Rivers">
    <option>Abua/Odual</option>
    <option>Ahoada East</option>
    <option>Ahoada West</option>
    <option>Akuku Toru</option>
    <option>Andoni</option>
    <option>Asari-Toru</option>
    <option>Bonny</option>
    <option>Degema</option>
    <option>Emohua</option>
    <option>Eleme</option>
    <option>Etche</option>
    <option>Gokana</option>
    <option>Ikwerre</option>
    <option>Khana</option>
    <option>Obia/Akpor</option>
    <option>Ogba/Egbema/Ndoni</option>
    <option>Ogu/Bolo</option>
    <option>Okrika</option>
    <option>Omumma</option>
    <option>Opobo/Nkoro</option>
    <option>Oyigbo</option>
    <option>Port-Harcourt</option>
    <option>Tai</option>
</div>

<div style="display: none;" id="Sokoto">
    <option>Binji</option>
    <option>Bodinga</option>
    <option>Dange-shnsi</option>
    <option>Gada</option>
    <option>Goronyo</option>
    <option>Gudu</option>
    <option>Gawabawa</option>
    <option>Illela</option>
    <option>Isa</option>
    <option>Kware</option>
    <option>kebbe</option>
    <option>Rabah</option>
    <option>Sabon birni</option>
    <option>Shagari</option>
    <option>Silame</option>
    <option>Sokoto North</option>
    <option>Sokoto South</option>
    <option>Tambuwal</option>
    <option>Tqngaza</option>
    <option>Tureta</option>
    <option>Wamako</option>
    <option>Wurno</option>
    <option>Yabo</option>
</div>

<div style="display: none;" id="Taraba">
    <option>Ardo-kola</option>
    <option>Bali</option>
    <option>Donga</option>
    <option>Gashaka</option>
    <option>Cassol</option>
    <option>Ibi</option>
    <option>Jalingo</option>
    <option>Karin-Lamido</option>
    <option>Kurmi</option>
    <option>Lau</option>
    <option>Sardauna</option>
    <option>Takum</option>
    <option>Ussa</option>
    <option>Wukari</option>
    <option>Yorro</option>
    <option>Zing</option>
</div>

<div style="display: none;" id="Yobe">
    <option>Bade</option>
    <option>Bursari</option>
    <option>Damaturu</option>
    <option>Fika</option>
    <option>Fune</option>
    <option>Geidam</option>
    <option>Gujba</option>
    <option>Gulani</option>
    <option>Jakusko</option>
    <option>Karasuwa</option>
    <option>Karawa</option>
    <option>Machina</option>
    <option>Nangere</option>
    <option>Nguru Potiskum</option>
    <option>Tarmua</option>
    <option>Yunusari</option>
    <option>Yusufari</option>
</div>

<div style="display: none;" id="Zamfara">
    <option>Anka</option>
    <option>Bakura</option>
    <option>Birnin Magaji</option>
    <option>Bukkuyum</option>
    <option>Bungudu</option>
    <option>Gummi</option>
    <option>Gusau</option>
    <option>Kaura</option>
    <option>Namoda</option>
    <option>Maradun</option>
    <option>Maru</option>
    <option>Shinkafi</option>
    <option>Talata Mafara</option>
    <option>Tsafe</option>
    <option>Zurmi</option>
</div>

<script>

 $('#donor-state').on('change', function(data){
  var selected = $('#donor-state').val();
  var divcontent = $('#'+selected).html();
  $('#donor-lga').html(divcontent);
 });

 $('#partner-state').on('change', function(data){
  var selected = $('#partner-state').val();
  var divcontent = $('#'+selected).html();
  $('#partner-lga').html(divcontent);
 });

</script>

