

<link rel="stylesheet" type="text/css"  href="<?php echo Yii::app()->createAbsoluteUrl('/') ?>/css/smart-forms.css">
<link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"  href="<?php echo Yii::app()->createAbsoluteUrl('/') ?>/css/custom.css">

<style>
    body{background-color:#f5ffe8;}
    .header,.footer{display: none; }


</style>
<div class="container form-page">
    <div class="row">



        <div class="col-md-12 gfs-account" >             
            <div class="smart-wrap">

                <div class="smart-forms smart-container wrap-2" id="login">

                    <div class="form-header header-lite">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('/home') ?>">
                            <img class="logo" src="<?php echo Yii::app()->createAbsoluteUrl('/') ?>/assets/image/gfs-trans.png">
                        </a>
                        <h4 class="">Login</h4>
                    </div><!-- end .form-header section -->

                    <form method="post" action="<?php echo Yii::app()->createUrl('donor/login') ?>" id="login_Form">
                        <div class="form-body">
                            <?php if (isset($this->msg)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->msg ?>
                                </div>
                            <?php } ?>
                            <?php if (isset($this->err)) { ?>
                                <div class="alert alert-warning">
                                    <?php echo $this->err ?>
                                </div>
                            <?php } ?>
                            <div class="section">
                                <label for="account" class="field-label">Login As:</label>
                                <label class="field select">
                                    <select id="login_account" name="accountCategory" required >
                                        <option value="donor"> Donor</option>
                                        <option value="partner">Partner</option>
                                    </select>
                                    <i class="arrow double"></i>                    
                                </label>  
                            </div><!-- end section -->








                            <div class="section">
                                <label for="email" class="field-label">Your email address</label>
                                <label for="email" class="field prepend-icon">
                                    <input type="email" name="email" id="email" class="gui-input" placeholder="example@domain.com...">
                                    <label for="email" class="field-icon"><i class="fa fa-envelope"></i></label>  
                                </label>
                            </div><!-- end section -->                    

                            <div class="section">
                                <label for="password" class="field-label">password</label>
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="password" id="password" class="gui-input">
                                    <label for="password" class="field-icon"><i class="fa fa-lock"></i></label>  
                                </label>
                            </div><!-- end section -->






                        </div><!-- end .form-body section -->
                        <div class="form-footer">
                            <div class="col-md-4 center-grid">
                                <button type="submit" class="button btn-primary btn-main ">Login</button>
                            </div>
                            <p class="form-text"> Don't have an account ,
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('/site/register') ?>/" style="cursor:pointer;"> Register</a></p><!-- end .form-footer section -->
                        </div>

                    </form>

                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->


        </div>



    </div>
</div>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<script>
// $('.conditional').conditionize();
</script>


<script>



    $('#login_account').on('change', function () {
        var type = $('#login_account option:selected').val();
        if (type === 'donor') {
            console.log('donor');
            $("#login_Form").attr("action", '<?php echo Yii::app()->createUrl('donor/login') ?>');
            $("#donorField").show();
            $("#partnerField").hide();
        } else if (type === 'partner') {
            console.log('partner');
            $("#login_Form").attr("action", '<?php echo Yii::app()->createUrl('partner/login') ?>');
            $("#donorField").hide();
            $("#partnerField").show();
        }
    });

    $('#account').change();


</script>

