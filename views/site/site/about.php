<!DOCTYPE html>
<html>
<head>
    <title>Woome</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap single page template">

    <link type="text/css"  href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css"  href="css/font-awesome.min.css" rel="stylesheet">
    <link type="text/css" href="css/woome-interior-main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">

</head>
<body>
<div class="body-holder">
    

<div class="header">
    <div class=" container ">
        <div class="row">
            <div class="col-md-2 col-lg-2">
                <a href="<?php echo $this->createUrl('site/home') ?>" class="header__logo">
                    <img src="assets/image/woome-logo-white.png">
                </a>
            </div>
            <div class="col-md-10 col-lg-10 ">
                <ul class="nav navbar-nav navbar-right hidden-xs">
                    <li>
                        <label>
                            <input type="text" placeholder="Search">
                            <span class="addon">
                                <i class="fa fa-search"></i>
                            </span>
                        </label>
                    </li>
                    <li><a href="#">About</a></li>
                    <?php if(Yii::app()->user->isGuest){ ?>
                        <li><a href="<?php echo Yii::app()->createUrl('site/login') ?>">Login</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/register') ?>">Sign Up</a></li>
                    <?php }elseif(!Yii::app()->user->isGuest && (Yii::app()->user->userType) == 'donor'){ ?>
                        <li><a href="<?php echo Yii::app()->createUrl('donor/dashboard') ?>">Dashboard</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Logout</a></li>
                    <?php }elseif(!Yii::app()->user->isGuest && (Yii::app()->user->userType) == 'partner'){ ?>
                        <li><a href="<?php echo Yii::app()->createUrl('partner/dashboard') ?>">Dashboard</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Logout</a></li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container content-page">
    <div class="row">
        <div class="spacer"></div>
        <div class="col-md-offset-1 col-md-10 col-xs-12 text-center">
            <h1 class="big-title" id="about" >About GFS</h1>
            <h4 class="regular-text-left">GFS (Give Food Stamps) is a Social Enterprise Food based Program set up to provide access to cooked food to the less privileged and most vulnerable in the society while also creating business opportunities for food vendors serving the target demography.  The primary aim of the program is to help eradicate hunger and alleviate poverty in our society </h4>
            <h4 class="regular-text-left">One cannot over emphasise the need for food, water and shelter as the basic needs for man’s existence; whilst drinking water may be easier to access at no cost and shelter improvised, food almost always attracts a fee if not scavenged. Hence the GFS focus on food for maximum social impact.</h4>
            <h4 class="regular-text-left">The program makes use of meal vouchers also known as food stamps which are funded in collaboration with various organisations and individuals. The food stamps may be purchased by individuals or corporate bodies/organisations and distributed either directly by the donor or through accredited NGO's working in various communities across the participating countries. The beneficiaries may then redeem the food stamps for a hot meal from our network of food vendors spread out in various locations throughout the country</h4>
            <h4 class="regular-text-left">The program started as the first of its kind in Lagos, Nigeria where over 5,000 street food vendors have been enrolled unto the GFS Food Vendor Network. The enrolment provides the food vendor with financial inclusion by access to financial services and the ability to buy food at wholesale prices by leveraging on economies of scale</h4>
            <h4 class="regular-text-left">In 2012, statistics from the World Bank Sub-Saharan Africa Poverty and Equity study indicated that 501 million people, constituting 47% of the total population of sub-Saharan Africa, lived on $1.90 a day or less. In a world, where we produce enough food to feed everyone, up to 795 million people still go to bed on an empty stomach each night</h4>
            <h4 class="regular-text-left">This level of poverty is the principal factor causing widespread hunger and increased crime in the society. People around the world struggle to feed their children at least a meal a day. In a more recent publication by the United Nation’s World Food Program in July 2016, Nigeria was described as a ‘food deficit country'. Often referred to as the “Giant of Africa’” because of its large population, Nigeria has approximately 184,000,000 inhabitants with about 70.8% of the population living below the one-dollar-a-day poverty threshold</h4>
            <h4 class="regular-text-left">The objective of GFS is guided by its social impact which is to make sure that one less person goes to bed hungry. GFS understands that the hunger problem in Nigeria and the world is not the sole responsibility of government but the combined responsibility of all both corporate and individual lending a hand to make sure that the less privileged living below $1 a day can get a hot meal a day from their local canteens</h4>
            
            
            <h1 class="big-title" id="howitworks" >How it Works</h1>
            <h4 class="regular-text-left">The feeding program works in five simple steps:
<ul>
    <li>Donor sponsors food stamps</li>
    <li>Food stamps are distributed to beneficiaries</li>
<li>Beneficiaries redeem at participating food canteens</li>
<li>Food vendor receive and validate the food stamps before serving food</li>
<li>At close of business, the bank credits the food vendors daily for food stamps redeemed.</li>
</ul>
</h4>

            <h4 class="regular-text-left">On the GFS Platform, Donors may determine and restrict the regions for which their food stamps are distributed and can also monitor the redemption of the food stamps in real-time.</h4>
            <h4 class="regular-text-left">Under this program there is an assurance that the beneficiary will consume a hot meal as each food stamp is tracked by geographical location and by the redeeming food vendor.</h4>
            <h4 class="regular-text-left">The food vendors may also track their redemptions at any time of the day using their phones.</h4>
 

            <h1 class="big-title" id="stakeholders" >Stakeholders</h1>
            <h4 class="regular-text-left"><b>Donors:</b></h4>
            <h4 class="regular-text-left">The Food Stamps are paid for by the Donors. Donors comprises of Government, Multinational Donor Agencies, Corporate Organization and benevolent individual. Donors can purchase food stamps on this website and choose an NGO to distribute the stamps to the beneficiaries or can decide to distribute the food stamps themselves.. <a style="color: green" href="<?php echo $this->createUrl('donor/login') ?>">Become Donor</a></h4>
            <h4 class="regular-text-left"><b>NGO (Partners) :</b></h4>
            <h4 class="regular-text-left">Non-Governmental Organisations are validated for their appropriate registrations and address verification before they are published on the website. Only after the validation and verification exercise shall the NGO be allowed to publish their work with the communities for which they solicit the feeding program.. <a style="color: green" href="<?php echo $this->createUrl('partner/login') ?>">Become Partner</a></h4>
            <h4 class="regular-text-left"><b>FOOD VENDOR NETWORK :</b></h4>
            <h4 class="regular-text-left">For enquiries on joining the GFS food network or bringing the food network to your city, please email info@givefoodstamps.com</h4>
            <h4 class="regular-text-left"><b>BENEFICIARY :</b></h4>
            <h4 class="regular-text-left">The beneficiaries are indigent and less privileged in the society living below $1 a day</h4>
            
            
            <h1 class="big-title" id="board" >Board</h1>
            <h4 class="regular-text-left"><b>Wumi Oghoetuoma (Founder/Chairman)</b></h4>
            <h4 class="regular-text-left">A graduate of Computer Science from Kingston University in the UK in 2001</h4>
            <h4 class="regular-text-left">He built his career as a Business Analyst and has been a Technology Solutions Designer across Europe, Australia & Africa for top companies such as AOL, Virgin Media, Tele2, Thomson Reuters & Altech Multimedia and more</h4>
            <h4 class="regular-text-left">He founded Crown Interactive where he still serves as Director and raised equity investment as a technology entrepreneur and moved to Nigeria to design and implement proudly Nigerian Solutions for Developing Country Issues in the public and private sector. Wumi leads the design of solutions in the telecoms and power sector and now for citizens’ welfare</h4>
            <h4 class="regular-text-left"><b>Toyin Oghoetuoma (Co-Founder)</b></h4>
            <h4 class="regular-text-left">A graduate of Business Information Technology at London Metropolitan University in the UK in 2002</h4>
            <h4 class="regular-text-left">Her first job as a graduate was in one of the top banks in United Kingdom in Business Operations. She worked there for some years until she ventured into entrepreneurship and relocated to Nigeria in 2012</h4>
            <h4 class="regular-text-left">A passionate media enthusiast spearheading online media interactivity for major global clients. She is the CEO of Tozaza Media Ltd, a media company in Lagos Nigeria that provides online multimedia and streaming solutions to corporate companies</h4>
            <h4 class="regular-text-left">She is passionate about children’s lifestyle in education and has therefore participated at executive levels in Parents Advisory Committees for schools. Toyin has a passion for social enterprise with a focus on innovative food distribution for poverty alleviation</h4>
            <h4 class="regular-text-left"><b>Chinwe Onubogu LLM, ACIS (Co-Founder)</b></h4>
            <h4 class="regular-text-left">A practicing Solicitor and Chartered Secretary based in Lagos, Nigeria. She obtained a degree in Law (LLB) from Enugu State University of Science and Technology in 2002. She was admitted to practice as a Barrister and Solicitor of the Supreme Court of Nigeria in 2005</h4>
            <h4 class="regular-text-left">She holds a Master’s Degree (LLM) in Commercial law from University of Witwatersrand, Johannesburg SA</h4>
            <h4 class="regular-text-left">She is the Founder and Managing Director of Zuriel Consulting Limited and Managing Partner of ZCL Solicitors a legal consulting and professional services firm with a focus on SMEs.  She has a passion for social enterprise and high impact investments</h4>
            
            <h1 class="big-title" id="management" >Management</h1>
            <h4 class="regular-text-left"><b>Oyin Idowu (Project Coordinator)</b></h4>
            <h4 class="regular-text-left">Oyin Idowu graduated from the University of Ibadan with a degree in Law and was called to the Nigerian Bar in 2004. She also has an LLM in International Trade law from Hull University UK</h4>
            <h4 class="regular-text-left">Oyin initially started her career as a market researcher in the United Kingdom for Kantar Operations (a division of Millward Brown) before focusing her career on legal compliance and human resource management in the petroleum and agricultural sectors in Nigeria</h4>
            <h4 class="regular-text-left">Oyin is passionate about women empowerment in developing countries and seeks to lend a voice in creating financial freedom for women at the bottom of the pyramid</h4>
            
            <h1 class="big-title" id="contact" >Contact Us</h1>
            <h4 class="regular-text-left">Address : 45 Admiralty Way, Lekki Phase 1</h4>
            <h4 class="regular-text-left">Email : Info@givefoodstamps.com</h4>
            <h4 class="regular-text-left">Mobile : +234907 811 8621</h4>
            
            <h1 class="big-title" id="partners" >Our Partners</h1>
            <h4 class="regular-text-left">
                <ul>
                    <li>Woome Distribution Systems</li>
                    <li>Tozaza Media</li>
                    <li>Naija Malls</li>
                    <li>Zuriel Consulting</li>
                </ul>
            </h4>
            
            
            <div class="spacer"></div>
        </div>
        <div class="col-md-3 col-sm-2"></div>
    </div>
</div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer__menu">
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#about">About</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#howitworks">How It works</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#stakeholders">Stakeholders</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#board">Board</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#management">Management</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#contact">Contact Us</a>
                    <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#partners">Partners</a>
                </div>
            </div>
            <div class="col-md-12">
            </div>
            <div class="col-md-12 footer__copyright">
                Copyright Givefoodstamps &copy; <?php echo '2016 - '.date('Y')?>
            </div>
            <div class="col-md-12 footer__socials">
                <a href="#"><i class="fa fa-facebook-square"></i> </a>
                <a href="#"><i class="fa fa-twitter"></i> </a>
                <a href="#"><i class="fa fa-instagram"></i> </a>
                <a href="#"><i class="fa fa-google-plus"></i> </a>
            </div>
        </div>
    </div>
</footer></body>

</html>