<!DOCTYPE html>
<html>
    <head>
        <title>Woome</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap single page template">

        <link type="text/css"  href="css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css"  href="css/font-awesome.min.css" rel="stylesheet">
        <link type="text/css" href="css/woome-interior-main.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
      
         <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }
        </style>
        
        
    </head>
    <body>
        <div class="body-holder">


            <div class="header header__holo">
                <div class=" container ">
                    <div class="row">
                        <div class="col-md-2 col-lg-2">
                            <a href="#" class="header__logo">
                                <img src="assets/image/woome-logo-01.png">
                            </a>
                        </div>
                       
                            <ul class="nav navbar-nav navbar-right">
                               
                                <li><a href="#">About</a></li>
                                <?php if (Yii::app()->user->isGuest) { ?>
                                    <li><a href="<?php echo Yii::app()->createUrl('site/login') ?>">Login</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('site/register') ?>">Sign Up</a></li>
                                <?php } elseif (!Yii::app()->user->isGuest && (Yii::app()->user->userType) == 'donor') { ?>
                                    <li><a href="<?php echo Yii::app()->createUrl('donor/dashboard') ?>">Dashboard</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Logout</a></li>
                                <?php } elseif (!Yii::app()->user->isGuest && (Yii::app()->user->userType) == 'partner') { ?>
                                    <li><a href="<?php echo Yii::app()->createUrl('partner/dashboard') ?>">Dashboard</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Logout</a></li>
                                <?php } ?>
                            </ul>
                       
                    </div>
                </div>
            </div>
            
            
     
            
            
          <!--  <section class="home-section jumbotroon" style="background: url(assets/image/heroBanner1.jpg); padding: 150px 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-center">
                            <h1>A synergic Approach to Eradicating Hunger</h1>
                            <h4>GFS (Give Food Stamps) is a Social Enterprise Food based Program.   The primary aim of the

                                program is to help eradicate hunger, alleviate poverty and increase financial inclusion for the

                                bottom of the pyramid in our society.</h4>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-4 col-xs-12">
                                    <a href="< ?php echo Yii::app()->createUrl('site/register') ?>" class="button-blue">Start Campaign</a>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <a href="#" class="button-transparent">Learn More</a>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </section>-->
            
            
            <section class="hero">
                
                <div class="">
                   <div class="row">
                <div class="col-md-12">
                    <div class="slider bg-grey" id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto; width:1440px !important; height:642px !important; overflow: hidden;">
                
                 
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
            </div>

            <!-- Slides Container -->
            <div u="slides" style="position: absolute; left: 0px; top: 0px; width:1440px; height:642px;
            overflow: hidden;">
                <div>
                    <img data-u="image" src="assets/image/heroBanner1.jpg" />
                </div>
                 <div>
                    <img data-u="image" src="assets/image/heroBanner1.jpg" />
                </div>
                 <div>
                    <img data-u="image" src="assets/image/heroBanner1.jpg" />
                </div>
                <div>
                    <img data-u="image" src="assets/image/heroBanner1.jpg" />
                </div>
            </div>
            
            <!--#region Bullet Navigator Skin Begin -->
            <!-- Help: https://www.jssor.com/development/slider-with-bullet-navigator.html -->
            <style>
                .jssorb031 {position:absolute;}
                .jssorb031 .i {position:absolute;cursor:pointer;}
                .jssorb031 .i .b {fill:#000;fill-opacity:0.5;stroke:#fff;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.3;}
                .jssorb031 .i:hover .b {fill:#fff;fill-opacity:.7;stroke:#000;stroke-opacity:.5;}
                .jssorb031 .iav .b {fill:#fff;stroke:#000;fill-opacity:1;}
                .jssorb031 .i.idn {opacity:.3;}
            </style>
            <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!--#endregion Bullet Navigator Skin End -->
        
            <!--#region Arrow Navigator Skin Begin -->
            <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html -->
            <style>
                .jssora051 {display:block;position:absolute;cursor:pointer;}
                .jssora051 .a {fill:none;stroke:#a8d05b;stroke-width:360;stroke-miterlimit:10;}
                .jssora051:hover {opacity:.8;}
                .jssora051.jssora051dn {opacity:.5;}
                .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
            </style>
            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->

                      
                        
                           
                      
        </div>
        <!-- Jssor Slider End -->
                </div>
            
                    </div>
            </div>
                <div class="row hidden-xs">
                    <div class="container quick-btn">
                        <div class="col-md-3 col-xs-6 btn1">
                           <a ><i class="fa fa-heart" aria-hidden="true"></i>Start A Campaign</a>
                        </div>
                        <div class="col-md-3 col-xs-6 btn2">
                             <a ><i class="fa fa-credit-card" aria-hidden="true"></i>Donate Food Voucher</a>
                            
                        </div>
                        <div class="col-md-3 col-xs-6 btn3">
                            <a ><i class="fa fa-cutlery" aria-hidden="true"></i>Become Food Vendor</a>
                        </div>
                        <div class="col-md-3 col-xs-6 btn4">
                            <a ><i class="fa fa-external-link-square" aria-hidden="true"></i>Learn More</a>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="">
                        <div class="col-md-12 bar">
                             
                        </div>
                 
                    </div>
                </div>  
            </section> 
            
            <section class="home-section how-it-works " style="">
                <div class="container ">
                    <div class="row">
                        <h1 class="text-center">How It Works
                            <small>Simplified food charity system</small>
                        </h1>
                    </div>
                    <div class="spacer"></div>
                    <div class="row text-center how-it-works">
                        <div class="col-sm-3 col-xs-12 how-it-works__item item">
                            <figure class="how-image">
                                <img src="assets/image/gfs-partner-01.png" width="40%">
                            </figure>
                            <h5>NGOS (Partners)</h5>
                            <p>
                                partner Create a Campaign and generate Food stamps   </p>

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item item">
                            <figure class="how-image">
                                <img src="assets/image/payment.png" width="40%">
                            </figure>
                            <h5>Donors</h5>
                            <p>
                               The Food Stamps are paid for by the Donors. Donors comprises of individuals or Government 

                            </p>

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item">
                            <figure class="how-image">
                                <img src="assets/image/people.png" width="40%">
                            </figure>
                            <h5>Beneficiaries</h5>
                            <p>

                                 Food stamps are distributed to beneficiaries ,The beneficiaries are indigent and less privileged in the society living below $1 a day </p>           

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item">
                            <figure class="how-image">
                                <img src="assets/image/vendor.png" width="40%">
                            </figure>
                            <h5>Food Vendors</h5>
                            <p>

                                Beneficiaries redeem at participating food canteens,Food vendor receive and validate the food stamps before serving food </p>

                             

                        </div>
                    </div>
                </div>
            </section>

            
            
            <section class="home-section jumbotroon" style="background: url(assets/image/videobg.png);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2 col-xs-12 text-center">
                            <h1>Watch the Video</h1>
                            <h4>See the process and success stories</h4>
                            <span class="btn-holder">
                                <a href="#" class="play-button"><i class="fa fa-play"></i>PLAY</a>
                            </span>
                        </div>
                    </div>
                </div>
            </section>

            <section class="home-section ">
                <div class="container">
                    <div class="row">
                        <h1 class="text-center">Featured Campaign</h1>
                    </div>
                    <div class="spacer"></div>
                    <div class="row text-center ">
                        <div class="col-sm-3 col-xs-12 ">
                            <div class="feat-camp">
                                <div class="camp-img">
                                
                                    <figure>
                                    <img src="assets/image/flood-victim.png">
                                    </figure>
                                 <div class="title">
                                     <h1><a href="#">Flood Victim relief In Kogi <br> <span class="partner">Access Bank</span> </a></h1>
                                </div>
                                </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, aque ipsa quae ab illo inventore
                            </p>
                            
                            <div class="donate">
                                 <div class="  btn2">
                                 <a >Donate Food Voucher</a>
                                </div>
                                <div class="  btn2 btn2-outline">
                                 <a >Learn More</a>
                                </div>
                            </div>
                        </div>
                        </div>
                         
                       
                          <div class="col-sm-3 col-xs-12 ">
                            <div class="feat-camp">
                                <div class="camp-img">
                                
                                    <figure>
                                    <img src="assets/image/flood-victim.png">
                                    </figure>
                                 <div class="title">
                                     <h1><a href="#">Flood Victim relief In Kogi <br> <span class="partner">Access Bank</span> </a></h1>
                                </div>
                                </div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, aque ipsa quae ab illo inventore
                            </p>
                            
                            <div class="donate">
                                 <div class="  btn2">
                                 <a >Donate Food Voucher</a>
                                </div>
                                <div class="  btn2 btn2-outline">
                                 <a >Learn More</a>
                                </div>
                            </div>
                        </div>
                        </div>
                          <div class="col-sm-3 col-xs-12 ">
                            <div class="feat-camp">
                                <div class="camp-img">
                                
                                    <figure>
                                    <img src="assets/image/flood-victim.png">
                                    </figure>
                                 <div class="title">
                                     <h1><a href="#">Flood Victim relief In Kogi <br> <span class="partner">Access Bank</span> </a></h1>
                                </div>
                                </div>
                            
                            
                           
                            
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, aque ipsa quae ab illo inventore
                            </p>
                            
                            <div class="donate">
                                 <div class="  btn2">
                                 <a >Donate Food Voucher</a>
                                </div>
                                <div class="  btn2 btn2-outline">
                                 <a >Learn More</a>
                                </div>
                            </div>
                        </div>
                        </div>
                          <div class="col-sm-3 col-xs-12 ">
                            <div class="feat-camp">
                                <div class="camp-img">
                                
                                    <figure>
                                    <img src="assets/image/flood-victim.png">
                                    </figure>
                                 <div class="title">
                                     <h1><a href="#">Flood Victim relief In Kogi <br> <span class="partner">Access Bank</span> </a></h1>
                                </div>
                                </div>
                            
                            
                           
                            
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, aque ipsa quae ab illo inventore
                            </p>
                            
                            <div class="donate">
                                 <div class="  btn2">
                                 <a >Donate Food Voucher</a>
                                </div>
                                <div class="  btn2 btn2-outline">
                                 <a >Learn More</a>
                                </div>
                            </div>
                        </div>
                        </div>
                      
                    </div>
                    <div class="col-md-8" style="float:none; margin:0px auto;  margin-top:40px;">
                    <div class="">
                         <a class="btn btn-outline-primary btn-lg btn-block btn-light-outline" >See More Campaigns</a>
                        </div>
                    </div>
                    
                </div>
            </section>

            <section class="home-section white">
                <div class="container partner">
                    <div class="row text-center">
                        <h1 class="col-xs-12">Our Partners</h1>
                        <div class="col-sm-4 col-xs-12 partner__item">
                            <img src="assets/image/naijamall.jpg">
                        </div>
                        <div class="col-sm-4 col-xs-12 partner__item">
                            <img src="assets/image/tozaza.png">
                        </div>
                        
                        <div class="col-xs-12"><p><a href="<?php echo Yii::app()->createUrl('site/register') ?>">Become a Channel Partner</a></p></div>
                    </div>
                </div>
            </section>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer__menu">
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#about">About</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#howitworks">How It works</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#stakeholders">Stakeholders</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#board">Board</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#management">Management</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#contact">Contact Us</a>
                            <a href="<?php echo $this->createAbsoluteUrl('site/about') ?>#partners">Partners</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                    </div>
                    <div class="col-md-12 footer__copyright">
                        Copyright givefoodstamps &copy; <?php echo '2016 - ' . date('Y') ?>
                    </div>
                    <div class="col-md-12 footer__socials">
                        <a href="#"><i class="fa fa-facebook-square"></i> </a>
                        <a href="#"><i class="fa fa-twitter"></i> </a>
                        <a href="#"><i class="fa fa-instagram"></i> </a>
                        <a href="#"><i class="fa fa-google-plus"></i> </a>
                    </div>
                </div>
            </div>
        </footer>
     <script src="<?php echo Yii::app()->createAbsoluteUrl('/') ?>/js/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->createAbsoluteUrl('/') ?>/js/jssor.slider.min.js"></script>
     
    <script>

        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: 1,                                       //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $Idle: 2000,                                        //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: 1,   			            //[Optional] Steps to go for each navigation request by pressing arrow key, default value is 1.
                $SlideEasing: $Jease$.$OutQuint,                    //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
                $SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide, default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $Cols: 1,                                           //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $Align: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                },

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Rows: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$ScaleWidth(parentWidth - 30);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
    
    </body>

</html>