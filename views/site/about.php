
    
    <div class="container">
        <div class="wrapper">
            <div class="row row-offcanvas row-offcanvas-left gfs-inner">
        <!-- sidebar -->
        <div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
            <ul class="nav" id="menu">
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>" class="active"><span class="collapse in hidden-xs">About</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>" > <span class="collapse in hidden-xs">How it Works</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/stakeholders') ?>"> <span class="collapse in hidden-xs">Stakeholders</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/board') ?>"><span class="collapse in hidden-xs">Board</span></a></li>
                
           </ul>
        </div>
        <!-- /sidebar -->

        <!-- main right col -->
        <div class="column col-sm-9 col-xs-12" id="main">
            <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a>
           <h1 class="big-title" id="about" >About GFS</h1>
            <hr/>
            <article class="regular-text-left">
                <p>GFS (Give Food Stamps) is a Social Enterprise Food based Program set up to provide access to cooked food to the less privileged and most vulnerable in the society while also creating business opportunities for food vendors serving the target demography.  The primary aim of The program is to help eradicate hunger and alleviate poverty in our society,One cannot over emphasise the need for food, water and shelter as the gfs needs for man’s existence; whilst drinking water may be easier to access at no cost and shelter improvised, food almost always attracts a fee if not scavenged. Hence the GFS focus on food for maximum social impact.</p>
                
                <p>The program makes use of meal vouchers also known as food stamps which are funded in collaboration with various organisations and individuals. The food stamps may be purchased by individuals or corporate bodies/organisations and distributed either directly by the donor or through accredited NGO's working in various communities across the participating countries. The beneficiaries may then redeem the food stamps for a hot meal from our network of food vendors spread out in various locations throughout the country</p>
                <p>The program started as the first of its kind in Lagos, Nigeria where over 5,000 street food vendors have been enrolled unto the GFS Food Vendor Network. The enrolment provides the food vendor with financial inclusion by access to financial services and the ability to buy food at wholesale prices by leveraging on economies of scale</p>
                <p>In 2012, statistics from the World Bank Sub-Saharan Africa Poverty and Equity study indicated that 501 million people, constituting 47% of the total population of sub-Saharan Africa, lived on $1.90 a day or less. In a world, where we produce enough food to feed everyone, up to 795 million people still go to bed on an empty stomach each night</p>
                <p>This level of poverty is the principal factor causing widespread hunger and increased crime in the society. People around the world struggle to feed their children at least a meal a day. In a more recent publication by the United Nation’s World Food Program in July 2016, Nigeria was described as a ‘food deficit country'. Often referred to as the “Giant of Africa’” because of its large population, Nigeria has approximately 184,000,000 inhabitants with about 70.8% of the population living below the one-dollar-a-day poverty threshold</p>
                <p> The objective of GFS is guided by its social impact which is to make sure that one less person goes to bed hungry. GFS understands that the hunger problem in Nigeria and the world is not the sole responsibility of government but the combined responsibility of all both corporate and individual lending a hand to make sure that the less privileged living below $1 a day can get a hot meal a day from their local canteens. </p>
            </article>
        </div>
        <!-- /main -->
    </div>
</div>
        
        
        
        
    </div>
    
    
    
    
    
    