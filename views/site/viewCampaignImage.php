<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

?>

<style>
  div.gallery {
    margin: 5px;

    float: left;
    width: 180px;
  }



  div.gallery img {
    width: 100%;
    height: auto;
  }


  div.desc {
    padding: 15px;
    text-align: center;
  }

  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  #myImg:hover {
    opacity: 0.7;
  }

  /* The Modal (background) */
  .modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 1;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100%;
    /* Full height */
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.9);
    /* Black w/ opacity */
  }

  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }

  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation */
  .modal-content,
  #caption {
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @-webkit-keyframes zoom {
    from {
      -webkit-transform: scale(0)
    }

    to {
      -webkit-transform: scale(1)
    }
  }

  @keyframes zoom {
    from {
      transform: scale(0)
    }

    to {
      transform: scale(1)
    }
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px) {
    .modal-content {
      width: 100%;
    }
  }
</style>


<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="pagetitle">
        <span class="pagetitle__title">
          <a href="<?= Yii::$app->urlManager->createUrl('dashboard') ?>">Campaign /</a><span class="camp-title"><?php echo $campaign->title ?> </span>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="campaign-details">
        <div class="col-md-4 campaign-details__right-holder">
          <div class="campaign-details__right-holder">
            <span class="campaign-details__text">
            </span>
          </div>
        </div>
        <h2> <?= $campaign->title ?></h2>
        <div class="col-md-6">
          <div class="campaign-details__main">
            <p class=""><span>Created On: </span><?= date('F d, Y', strtotime($campaign->created_time)) ?></p>
            <p class=""><span>Description: </span><?= $campaign->description ?></p>
          </div>
        </div>
      </div>
    </div>
    <!-- Campaign details -->
    <div class="container">
      <div class="row">
        <div class="recent-dtn col-md-12" style="margin-bottom: 10px;">
          <h1> Images</h1>
          <div class="images row">
            <div class="col-md-12">
            <?php
            foreach ($imageGallery->getModels() as $image) { ?>
            
              <div class="gallery" style="margin-right: auto; margin-left: auto;padding: 10px 3px"> 
                <img class="myImg" src="<?= Yii::$app->request->baseUrl . '/OpenImage/readFile?imgt=' . $image->token; ?>" alt="campaign photos" style="text-align: center;">
              </div>
              
            <?php } ?>
            </div>
          </div>

        </div>
        <?= LinkPager::widget(['pagination' => $pagination,]); ?>
      </div>
      <!---End of row-->
      
    </div>
    <!---End of Container-->

   

    

    <!-- The Modal -->
    <div id="myModal" class="modal">
      <span class="close">&times;</span>
      <img class="modal-content" id="img01">
      <div id="caption"></div>
    </div>
  </div>
</div>




</div>

<script>
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  var img = document.getElementById("myImg");
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  /* img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  } */


  $(".myImg").on('click', function() {
    modal.style.display = "block";
    modalImg.src = this.src;
    console.log(this.src);
    captionText.innerHTML = this.alt;
  });


  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }
</script>