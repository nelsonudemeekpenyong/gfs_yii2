<!DOCTYPE html>
<html>
    <head>
        <title>Woome</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap single page template">

        <link type="text/css"  href="http://localhost/gfs/web/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css"  href="http://localhost/gfs/web/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link type="text/css"  href="http://localhost/gfs/web/css/woome-interior-main.css" rel="stylesheet">


        <link rel="stylesheet" href="http://localhost/gfs/web/css/custom.css">
        <link rel="stylesheet" href="http://localhost/gfs/web/css/smart-forms.css">

        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

        <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }


            .jssorb032 {position:absolute;}
            .jssorb032 .i {position:absolute;cursor:pointer;}
            .jssorb032 .i .b {fill:#fff;fill-opacity:0.7;stroke:#000;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.25;}
            .jssorb032 .i:hover .b {fill:#000;fill-opacity:.6;stroke:#fff;stroke-opacity:.35;}
            .jssorb032 .iav .b {fill:#000;fill-opacity:1;stroke:#fff;stroke-opacity:.35;}
            .jssorb032 .i.idn {opacity:.3;}

            .jssora051 {display:block;position:absolute;cursor:pointer;}
            .jssora051 .a {fill:none;stroke:#94C842;stroke-width:360;stroke-miterlimit:10;}
            .jssora051:hover {opacity:.8;}
            .jssora051.jssora051dn {opacity:.5;}
            .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
        </style>

    </head>
    <body>
        <div class="body-holder">
            <section class="hero home-section">
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;visibility:hidden;" class="slider">
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
                        <div>
                         <a href="https://www.givefoodstamps.com/viewcampaign?token=5e7e2654989a35e7e2654989eb">   <img data-u="image" 
                            src="http://localhost/gfs/web/assets/image/banners/IMG_9986.jpg" /> </a>
                        </div>
                        <div>
                         <a href="<?php echo Yii::$app->urlManager->createUrl('donor/topup_account'); ?>">   <img data-u="image" 
                            src="http://localhost/gfs/web/assets/image/banners/IMG_9987.jpg" /> </a>
                        </div>
                        <div>
                         <a href="<?php echo Yii::$app->urlManager->createUrl('donor/topup_account');  ?>">   <img data-u="image" 
                            src="http://localhost/gfs/web/assets/image/banners/IMG_9988.jpg" /> </a>
                        </div>
                        <div>
                         <a href="<?php echo Yii::$app->urlManager->createUrl('donor/topup_account'); ?>">   <img data-u="image" 
                            src="http://localhost/gfs/web/assets/image/banners/IMG_9989.jpg" /> </a>
                        </div>
                        <div>
                         <a href="<?php echo Yii::$app->urlManager->createUrl('donor/topup_account'); ?>">   <img data-u="image" 
                            src="http://localhost/gfs/web/assets/image/banners/IMG_9991.jpg" /> </a>
                        </div>
                    </div>
                    <!-- Bullet Navigator -->
                    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                        <div data-u="prototype" class="i" style="width:16px;height:16px;">
                            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                            </svg>
                        </div>
                    </div>
                    <!-- Arrow Navigator -->
                    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                        </svg>
                    </div>
                </div>
               
                <!-- <?php echo Yii::getAlias('@app')  . '/donor/createCampaign'  ?>" ><i class="fa fa-heart" aria-hidden="true"></i>Start A Campaign</a>
                        </div>
                        <div class="col-md-3 col-xs-6 btn2">
                            <a href="<?php echo Yii::$app->urlManager->createUrl('donor/topup_account') ?>"><i class="fa fa-credit-card" aria-hidden="true"></i>Donate Now</a>

                        </div>
                        <div class="col-md-3 col-xs-6 btn3">
                            <a href="<?php echo Yii::$app->urlManager->createUrl('/FoodVendor') ?>"><i class="fa fa-cutlery" aria-hidden="true"></i>Become Food Vendor</a>
                        </div>
                        <div class="col-md-3 col-xs-6 btn4">
                            <a href="<?php echo Yii::$app->urlManager->createUrl('site/about') ?>" ><i class="fa fa-external-link-square" aria-hidden="true"></i>Learn More</a>
                        </div>
                    </div>
                </div> --> 
                <div class="row">
                    <div class="">
                        <div class="col-md-12 bar">

                        </div>

                    </div>
                </div> 
            </section> 

            <div class="text-center">
                <button class=" btn btn-warning btn-lg active" style="margin-top: 25px; margin-bottom: -25px; border-radius: 5px; width: 300px;" >DONATE NOW </button>
            </div>

            <section class="home-section how-it-works " style="    padding: 25px 0;">
                <div class="container ">
                    <div class="row">
                        <h1 class="text-center">How It Works
                            <small>Simplified food distribution </small>
                        </h1>
                    </div>
                    <div class="spacer"></div>
                    <div class="row text-center how-it-works">
                        <div class="col-sm-3 col-xs-12 how-it-works__item item">
                            <figure class="how-image">
                                <img src="http://localhost/gfs/web/assets/image/gfs-partner-01.png" width="40%">
                            </figure>
                            <h5>NGOS (Partners)</h5>
                            <p>
                                partner Create a Campaign and generate Food stamps   </p>

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item item">
                            <figure class="how-image">
                                <img src="http://localhost/gfs/web/assets/image/payment.png" width="40%">
                            </figure>
                            <h5>Donors</h5>
                            <p>
                                The Food Stamps are paid for by the Donors. Donors comprises of individuals or Government 

                            </p>

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item">
                            <figure class="how-image">
                                <img src="http://localhost/gfs/web/assets/image/people.png" width="40%">
                            </figure>
                            <h5>Beneficiaries</h5>
                            <p>

                                Food stamps are distributed to beneficiaries ,The beneficiaries are indigent and less privileged in the society living below $1 a day </p>           

                        </div>
                        <div class="col-sm-3 col-xs-12 how-it-works__item">
                            <figure class="how-image">
                                <img src="http://localhost/gfs/web/assets/image/vendor.png" width="40%">
                            </figure>
                            <h5>Food Vendors</h5>
                            <p>
                                Beneficiaries redeem at participating food canteens,Food vendor receive and validate the food stamps before serving food </p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="home-section " style="background: url(http://localhost/gfs/web/assets/image/vision.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-1 col-xs-12 text-center mission">
                            <h1>Our Mission</h1>
                            <p>To provide access to cooked food for the less privileged and most vulnerable in the society while also creating business opportunities for food vendors serving the target demography.</p>
                            <div class="btn1">
                                <a href="<?php echo Yii::$app->urlManager->createUrl('site/register'); ?>" class=" btn"> &nbsp; Get Involved &nbsp; </a>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="home-section white">
                <div class="container partner">
                    <div class="row text-center">
                        <h1 class="col-xs-12">Supported by</h1>
                        <div class="col-md-12">
                            <div class="col-sm-3 col-xs-6 partner__item">
                                <img src="http://localhost/gfs/web/assets/image/woome.png ">
                            </div>
                            <div class="col-sm-3 col-xs-6 partner__item">
                                <img src="http://localhost/gfs/web/assets/image/tozaza.png">
                            </div>

                            <div class="col-sm-3 col-xs-6 partner__item">
                                <img src="http://localhost/gfs/web/assets/image/zuriel.png">
                            </div>
                            <div class="col-sm-3 col-xs-6 partner__item">
                                <img src="http://localhost/gfs/web/assets/image/access-bnk.png">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        
        <script src="http://localhost/gfs/web/js/jssor.slider.min.js"></script>
        <script src="http://localhost/gfs/web/js/jquery.min.js"></script>

        <script type="text/javascript">
            jssor_1_slider_init = function () {

                var jssor_1_SlideoTransitions = [
                    [{b: -1, d: 1, o: -0.7}],
                    [{b: 900, d: 2000, x: -379, e: {x: 7}}],
                    [{b: 900, d: 2000, x: -379, e: {x: 7}}],
                    [{b: -1, d: 1, o: -1, sX: 2, sY: 2}, {b: 0, d: 900, x: -171, y: -341, o: 1, sX: -2, sY: -2, e: {x: 3, y: 3, sX: 3, sY: 3}}, {b: 900, d: 1600, x: -283, o: -1, e: {x: 16}}]
                ];

                var jssor_1_options = {
                    $AutoPlay: 1,
                    $SlideDuration: 800,
                    $SlideEasing: $Jease$.$OutQuint,
                    $CaptionSliderOptions: {
                        $Class: $JssorCaptionSlideo$,
                        $Transitions: jssor_1_SlideoTransitions
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*#region responsive code begin*/

                var MAX_WIDTH = 3000;

                function ScaleSlider() {
                    var containerElement = jssor_1_slider.$Elmt.parentNode;
                    var containerWidth = containerElement.clientWidth;

                    if (containerWidth) {

                        var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                        jssor_1_slider.$ScaleWidth(expectedWidth);
                    } else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }

                ScaleSlider();

                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*#endregion responsive code end*/
            };
        </script>
        <script type="text/javascript">jssor_1_slider_init();</script>
    </body>

</html>
