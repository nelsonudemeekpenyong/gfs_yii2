<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            'email:email',
            'password',
            //'account_type',
            //'address',
            //'state',
            //'lga',
            //'token',
            //'last_login',
            //'user_type',
            //'organisation_name',
            //'organisation_category_id',
            //'description:ntext',
            //'operating_state',
            //'contact_number',
            //'parent_id',
            //'country_id',
            //'confirm_time',
            //'created_time',
            //'updated_time',
            //'is_active',
            //'is_approve',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
