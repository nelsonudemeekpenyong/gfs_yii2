<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\Users;
use app\models\Country;
use app\models\State;
use app\models\Lga;
use app\models\OrganisationCategory;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web') ?> . /css/smart-forms.css">
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::getAlias('@web') ?> . /css/custom.css">

<style>
    body {
        background-color: #f5ffe8;
    }

    .header,
    .footer {
        display: none;
    }
</style>


<div class="container form-page">
    <div class="row">
        <div class="col-md-12 gfs-account">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-2" id="register">

                    <div class="form-header header-lite">

                        <a href="<?= Yii::getAlias('@web') ?> ./ ">
                            <img class="logo" src="<?= Yii::getAlias('@web') ?>/assets/image/gfs-trans.png">
                        </a>
                        <h4 class="">Create Account</h4>
                    </div><!-- end .form-header section -->

                    <?php $form = ActiveForm::begin(); ?>
                    <div class="form-body">
                  

                        <?= $form->field($model, 'user_type')->dropDownList(['donor' => 'Donor', 'partner' => 'Partner'], ['prompt' => '--Register As--', 'id' => 'account', 'required' => 'required']); ?>
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'required' => 'required']) ?>
                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'required' => 'required']) ?>
                        <?= $form->field($model, 'email')->input('email', ['maxlength' => true, 'placeholder' => 'example@domain.com...', 'required' => 'required']) ?>
                        <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true, 'placeholder' => '08012345678', 'required' => 'required']) ?>
                        <?= $form->field($model, 'account_type')->dropDownList(ArrayHelper::map(Users::find()->all(), 'account_type', 'account_type'), ['prompt' => '--Select Account Type--', 'required' => 'required']); ?>
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(Country::find()->all(), 'id', 'name'), ['prompt' => '--Select Account Type--', 'value' => 'disabled', 'required' => 'required']); ?>


                        <div id="partnerField" style="display: none;">
                        <?= $form->field($model, 'organisation_name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'organisation_category_id')->dropDownList(ArrayHelper::map(OrganisationCategory::find()->all(), 'id', 'category'), ['prompt' => '--Select Category Type--', 'disabled']); ?>
                            <?= $form->field($model, 'description')->textarea(['rows' => 2, 'cols' => 5])->label('Describe Your Organization'); ?>
                        </div>

                        <div class="row">
                            <div class=" col-md-6"> <?= $form->field($model, 'state')->dropDownList(ArrayHelper::map(State::find()->all(), 'id', 'name'), ['prompt' => '--Select States--', 'required' => 'required']); ?> </div>
                            <div class=" col-md-6"> <?= $form->field($model, 'lga')->dropDownList(ArrayHelper::map(Lga::find()->all(), 'id', 'name'), ['prompt' => '--Select LGA--', 'required' => 'required']); ?> </div>
                        </div>

                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'placeholder' => 'Create a Password', 'required' => 'required']) ?>

                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" name="confirmPassword" class="form-control mb-5" required placeholder="Confirm Your Passwprd">
                        </div>

                        <div class="form-footer">
                            <div class="col-md-4 center-grid">
                        <?= Html::submitButton('Create Account', ['class' => 'button btn-primary btn-main']) ?>
                            </div>
                            <p class="form-text"> Already have an account , <a href="<?php //echo Yii::app()->createAbsoluteUrl('/site/login') ?>" onclick="showLoginForm()" style="cursor:pointer;"> Login</a></p>
                        </div><!-- end .form-footer section -->

                        <!-- <div class="form-group">
                       <?php //echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div> -->

                        <?php ActiveForm::end(); ?>
                    </div><!-- end .smart-forms section -->
                </div><!-- end .smart-wrap section -->
            </div>
        </div>
    </div>


    <script>


        $('#account').on('change', function () {
            var type = $('#account option:selected').val();
            if (type === 'donor') {
                console.log('donor');

                $("#donorField").show();
                $("#partnerField").hide();
            } else if (type === 'partner') {
                console.log('partner');

                $("#donorField").hide();
                $("#partnerField").show();
            }
        });

        $('#account').change();
    </script>


