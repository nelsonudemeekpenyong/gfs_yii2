<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstname',
            'lastname',
            'email:email',
            'password',
            'account_type',
            'address',
            'state',
            'lga',
            'token',
            'last_login',
            'user_type',
            'organisation_name',
            'organisation_category_id',
            'description:ntext',
            'operating_state',
            'contact_number',
            'parent_id',
            'country_id',
            'confirm_time',
            'created_time',
            'updated_time',
            'is_active',
            'is_approve',
            'updated_by',
        ],
    ]) ?>

</div>
