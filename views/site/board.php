

            <div class="container">
                <div class="wrapper">
                    <div class="row row-offcanvas row-offcanvas-left gfs-inner">
                        <!-- sidebar -->
                        <div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
                            <ul class="nav" id="menu">
                                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>" ><span class="collapse in hidden-xs">About</span></a></li>
                                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>" > <span class="collapse in hidden-xs">How it Works</span></a></li>
                                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/stakeholders') ?>"> <span class="collapse in hidden-xs">Stakeholders</span></a></li>
                                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/board') ?>" class="active"><span class="collapse in hidden-xs">Board</span></a></li>

                            </ul>
                        </div>
                        <!-- /sidebar -->

                        <!-- main right col -->
                        <div class="column col-sm-9 col-xs-12" id="main">
                            <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a>
                            <h1 class="big-title" id="about" >Board</h1>
                            <hr/>

                            <article class="regular-text-left">

                               <!-- <img style="" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/wumi.jpg"> -->
                                
                                
                                <h4 class="regular-text-left"><b>Wumi Oghoetuoma (Founder/Chairman)</b></h4>
                                <p class="regular-text-left">A graduate of Computer Science from Kingston University in the UK in 2001
                                    He built his career as a Business Analyst and has been a Technology Solutions Designer across Europe, Australia & Africa for top companies such as AOL, Virgin Media, Tele2, Thomson Reuters & Altech Multimedia and more
                                    He founded Crown Interactive where he still serves as Director and raised equity investment as a technology entrepreneur and moved to Nigeria to design and implement proudly Nigerian Solutions for Developing Country Issues in the public and private sector. Wumi leads the design of solutions in the telecoms and power sector and now for citizens’ welfare</p>

                                 <!--<img style=""float: left;" src="<?= Yii::$app->request->baseUrl; ?>/assets/image/w"> -->  
                                <h4 class="regular-text-left"><b>Toyin Oghoetuoma (Co-Founder)</b></h4>
                                <p class="regular-text-left">A graduate of Business Information Technology at London Metropolitan University in the UK in 2002
                                    Her first job as a graduate was in one of the top banks in United Kingdom in Business Operations. She worked there for some years until she ventured into entrepreneurship and relocated to Nigeria in 2012
                                    A passionate media enthusiast spearheading online media interactivity for major global clients. She is the CEO of Tozaza Media Ltd, a media company in Lagos Nigeria that provides online multimedia and streaming solutions to corporate companies
                                    She is passionate about children’s lifestyle in education and has therefore participated at executive levels in Parents Advisory Committees for schools. Toyin has a passion for social enterprise with a focus on innovative food distribution for poverty alleviation</p>

                                <h4 class="regular-text-left"><b>Chinwe Onubogu LLM, ACIS (Co-Founder)</b></h4>
                                <p class="regular-text-left">A practicing Solicitor and Chartered Secretary based in Lagos, Nigeria. She obtained a degree in Law (LLB) from Enugu State University of Science and Technology in 2002. She was admitted to practice as a Barrister and Solicitor of the Supreme Court of Nigeria in 2005
                                    She holds a Master’s Degree (LLM) in Commercial law from University of Witwatersrand, Johannesburg SA
                                    She is the Founder and Managing Director of Zuriel Consulting Limited and Managing Partner of ZCL Solicitors a legal consulting and professional services firm with a focus on SMEs.  She has a passion for social enterprise and high impact investments</p>
                            </article>
                        </div>
                        <!-- /main -->
                    </div>
                </div>
            </div>
