<?php
/*
 * 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* @var $this SiteController */
?>
<style>
    #map {
        width: 100%;
        height: 400px;
        background-color: grey;
    }
    .containerM{
        width: 54%;
        float: left;
        padding: 1px 1px 1px 100px;
        left: 75px;
        /* background-color: #eee; */
        top: 65%;

    }


    .Mapview{
        height: 400px;
        padding: 0 0 0 750px;
    }
    .boxed {
        border: 1px solid green ;
        width: 71%
    }


    #containerA {
        margin: 0 auto;
        width: 35%;
    }
    #accordion input {
        display: none;
    }
    #accordion label {
        background: #CFC;
        border-radius: .25em;
        cursor: pointer;
        display: block;
        margin-bottom: .125em;
        padding: 2.25em 1em;
        z-index: 20;
    }
    #accordion label:hover {
        background: #ccc;
    }

    #accordion input:checked + label {
        background: #ccc;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
        color: white;
        margin-bottom: 0;
    }
    #accordion article {
        background: #f7f7f7;
        height:0px;
        overflow:hidden;
        z-index:10;
    }
    #accordion article p {
        padding: 1em;
    }
    #accordion input:checked article {
    }
    #accordion input:checked ~ article {
        border-bottom-left-radius: .25em;
        border-bottom-right-radius: .25em;
        height: auto;
        margin-bottom: .125em;
    }



</style>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <span class="pagetitle__title">Food Vendors Network</span>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4"  style="margin-bottom: 10px;">
                <select id="country" class="form-control btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <option value="">--Select Country--</option>
                    <?php foreach ($countryLists as $countryList) { ?>
                          <option value="<?= $countryList->id ?>">
                          <?php echo $countryList->name . ' - vendors: '. $countryList->cnt ?>
                        </option>        
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-4" style="margin-bottom: 10px;">
                <select id="state"class="form-control  btn btn-secondary dropdown-toggle col-md-12"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <option value="">--Select State--</option>               
                    <?php foreach ($stateLists as $s) { ?>
                        <option <?php echo Yii::$app->request->get("state") == $s['id'] ? "selected" : "" ?> value="<?php echo $s['id']; ?>">
                            <?php echo $s['name'] . ' - vendors: ' . $s['cnt']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-4" style="margin-bottom: 10px;">
                <select id="lga" class="form-control  btn btn-secondary dropdown-toggle col-md-12"   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <option value="">--Select LGA--</option>
                    <?php foreach ($lga as $lg) { ?>
                        <option <?= Yii::$app->request->get("lga") == $lg['id'] ? "selected" : "" ?> value="<?= $lg['id']; ?>">
                            <?php echo $lg['name'] . ' - vendors: ' . $lg['cnt']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div id="demo" class="collapse">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" style="vertical-align:top;text-align:right;">
            <div id="map"></div>
        </div>
    </div>
</div>

<script>
    function initMap() {
        var locations = [];


        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: new google.maps.LatLng(6.5244, 3.3792),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        google.maps.event.addListener(map, 'dragend', function () {
            window.console.log("Map clicked");
        });



        var infowindow = new google.maps.InfoWindow();

        var markers, i;

        var image = {
            url: 'http://www.tow411.net/images/dotred.gif',
            size: new google.maps.Size(12, 12),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32)
        };

        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };


        var myUrl = $(location).attr('href');
        var vendorList = myUrl;
        vendorList = vendorList.replace("Map", "json")
        $.getJSON(vendorList, function (data) {
            var items = [];
            console.log(data);

            var i = 0;
            $.each(data, function (key, val) {
                if (i == 0) {
                    var center = new google.maps.LatLng(val.x_coordinate, val.y_coordinate)
                    map.panTo(center);
                }
                markers = new google.maps.Marker({
                    position: new google.maps.LatLng(val.x_coordinate, val.y_coordinate),
                    map: map,
                    icon: image,
                    // title: locations[i][0],
                    shape: shape
                });

                google.maps.event.addListener(markers, 'mouseover', (function (markers, i) {
                    return function () {
                        infowindow.setContent(val.first_name + " " + val.sur_name
                                + "<br>" + val.business_name
                                + "<br>" + val.addr_line_i + ", " + " " + val.landmark);
                        infowindow.open(map, markers);
                    }
                })(markers, i));
                i++
            });

        });

    }

    /* When the user clicks on the button, 
     toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("CountDrop").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("Count-drop");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }




    const currentURL = document.URL;

    var myUrl = $(location).attr('href');

    function addQSParm(name, value) {
        var re = new RegExp("([?&]" + name + "=)[^&]+", "");
        function add(sep) {
            myUrl += sep + name + "=" + encodeURIComponent(value);
        }

        function change() {
            myUrl = myUrl.replace(re, "$1" + encodeURIComponent(value));
        }
        if (myUrl.indexOf("?") === -1) {
            add("?");
        } else {
            if (re.test(myUrl)) {
                change();
            } else {
                add("&");
            }
        }
    }

    function removeQSParm(name) {
        var re = new RegExp("([?&]" + name + "=)[^&]+", "");
        myUrl = myUrl.replace(re, "");
    }

    viewTab = function (j, val) {

        switch (j) {
            case "country":
                console.log("country");
                addQSParm("country", val);
                break;
            case "state":
                addQSParm("state", val);
                console.log("state");
                break;
            case "lga":
                addQSParm("lga", val);
                console.log("lga");
                break;
        }
        location.href = myUrl;
        console.log(myUrl);
    };



    $(document).ready(function () {

        $('#country').change(function () {
            console.log($(this).val())
            viewTab("country", $(this).val())
        });

        $('#state').change(function () {
            viewTab("state", $(this).val())
        });

        $('#lga').change(function () {
            viewTab("lga", $(this).val())
        });
    });

</script>




<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCb9JS0wCQvknCAgr0EaB-KZZhe-a27sOA&callback=initMap">
</script>







