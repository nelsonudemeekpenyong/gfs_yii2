<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CamapignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\campaigns */

$this->title = 'Campaigns';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <span class="pagetitle__title">All Campaigns: <?php echo $dataProvider->getTotalCount() ?></span>
            </div>
        </div>
    </div>
</div>
<div class="container block-list">
    <div class="row">
        <div class="col-xs-12" style="margin-bottom: 10px">
            <!-- Start search div-->
            <div class="col-xs-12 block-list__list">

                <form class="form-horizontal" method="get">

                    <div class="form-group mx-sm-3" style="margin-left: 25px; margin-right: 25px;">

                        <label for="search" class="sr-only">Search</label>
                        <input type="text" class="form-control" id="search" name="CampaignSearch[title]" placeholder="Search Campaign" style="margin-bottom: 10px;">
                        <input type="submit" class="btn btn-warning btn-block " value="Search" name="search" />
                    </div>
                </form>
            </div> <!-- End search div-->

            <!-- End search Section -->
            <?php
            foreach ($models as $campaign) :
                $controllerId = 'site';
                $actionId = 'view-campaign';

                if (isset($user)) {
                    $actionId = ($user->id == $campaign->created_by) ? 'view-campaign' : 'view-campaign';
                    $controllerId = ($user->id == $campaign->created_by) ? $user->user_type : "site";
                }
            ?>

            <a href="<?= Yii::getAlias('@web') . "/$controllerId/$actionId?token=$campaign->token" ?>" class="block-list__list" style="margin-top: 7px;">
                    <?php if ($campaign->is_active == 1) { ?>
                        <div class="block-list__left-holder">
                            <i class="fa fa-play"></i>
                        </div>
                    <?php } else { ?>
                        <div class="block-list__left-holder">
                            <i class="fa fa-stop"></i>
                        </div>
                    <?php } ?>

                    <div class="block-list__main-holder">
                        <div class="block-list__title"><?php echo $campaign->title ?></div>
                        <div class="block-list__title">
                            <h5><?php echo $campaign->description ?></h5>
                        </div>
                        <div class="block-list__info">Created: <?php echo $campaign->created_time ?></div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
        <?= LinkPager::widget(['pagination' => $pagination,]); ?>
    </div>