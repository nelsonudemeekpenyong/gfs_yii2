<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" type="text/css"  href="<?php echo Yii::getAlias('@web') ?> . /css/smart-forms.css">
<link rel="stylesheet" type="text/css"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css"  href="<?php echo Yii::getAlias('@web') ?> . /css/custom.css">

<style>
    body{background-color:#f5ffe8;}
    .header,.footer{display: none; }


</style>

<div class="container form-page">
    <div class="row">
        <div class="col-md-12 gfs-account" >             
            <div class="smart-wrap">

                <div class="smart-forms smart-container wrap-2" id="login">

                    <div class="form-header header-lite">
                        <a href="<?=Yii::getAlias('@web') ?> . /home">
                            <img class="logo" src="<?= Yii::getAlias('@web') ?>/assets/image/gfs-trans.png">
                        </a>
                        <h4 class="">Login</h4>
                    </div><!-- end .form-header section -->

                    <form method="post"  id="login_Form" action="<?php //echo Yii::getAlias('@app')?> ">
                    <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/>
                        <div class="form-body">
                            <?php if (isset($this->msg)) { ?>
                                <div class="alert alert-success">
                                    <?php echo $this->msg ?>
                                </div>
                            <?php } ?>
                            <?php if (isset($this->err)) { ?>
                                <div class="alert alert-warning">
                                    <?php echo $this->err ?>
                                </div>
                            <?php } ?>
                            <div class="section">
                                <label for="account" class="field-label">Login As:</label>
                                <label class="field select">
                                    <select id="login_account" name="LoginForm[user_type]" required >
                                        <option value="donor"> Donor</option>
                                        <option value="partner">Partner</option>
                                    </select>
                                    <i class="arrow double"></i>                    
                                </label>  
                            </div><!-- end section -->

                            <div class="section">
                                <label for="email" class="field-label">Your email address</label>
                                <label for="email" class="field prepend-icon">
                                    <input type="email" name="LoginForm[username]" id="email" class="gui-input" placeholder="example@domain.com...">
                                    <label for="email" class="field-icon"><i class="fa fa-envelope"></i></label>  
                                </label>
                            </div><!-- end section -->           
                            
                            

                            <div class="section">
                                <label for="password" class="field-label">password</label>
                                <label for="password" class="field prepend-icon">
                                    <input type="password" name="LoginForm[password]" id="password" class="gui-input">
                                    <label for="password" class="field-icon"><i class="fa fa-lock"></i></label>  
                                </label>
                            </div><!-- end section -->






                        </div><!-- end .form-body section -->
                        <div class="form-footer">
                            <div class="col-md-4 center-grid">
                                <button type="submit" class="button btn-primary btn-main ">Login</button>
                            </div>
                            <p class="form-text"> Don't have an account ,
                                <a href="<?php echo Yii::getAlias('@web') ?>/site/register" style="cursor:pointer;"> Register</a></p><!-- end .form-footer section -->
                         <p class="form-text">     
                             <a href="<?php echo Yii::getAlias('@web') ?>/site/reset" style="cursor:pointer;"> Forget Password</a></p><!-- end .form-footer section -->   
                        </div>

                    </form>

                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->


        </div>



    </div>
</div>


