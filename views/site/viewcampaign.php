<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CamapignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $campaign app\models\campaigns */

$this->title = 'Campaigns';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <span class="pagetitle__title">
                    <a href="<?php // echo Yii::$app()->urlManager->createUrl('donor/dashboard') 
                                ?>">Campaign </a><span class="camp-title"> /<?php echo $campaign->title ?> </span>
                </span>
            </div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="campaign-details">
                <div class="col-md-4 campaign-details__right-holder">
                    <div class="campaign-details__right-holder">
                        <span class="campaign-details__text">
                        </span>
                    </div>
                </div>
                <h2> <?php echo $campaign->title ?></h2>
                <div class="col-md-6">
                    <div class="campaign-details__main">
                        <span>Donation: &nbsp; &nbsp; </span><?= ($totalDonations == 1) ? "${totalDonations} Foodstamp" : "${totalDonations} Foodstamps" ?>
                        <p class=""><span>Created On: </span><?= date('F d, Y', strtotime($campaign->created_time)) ?></p>
                        <p class=""><span>Description: </span><?= $campaign->description ?></p>
                        <p class=""><span>Location:&nbsp; &nbsp; &nbsp;</span> <?= $campaign->location ?></p>
                    </div>
                </div>
                <div class="col-md-6" style="display:flex; justify-content: space-around;">
                    <div>
                        <span class="campaign-details__text">
                            <?= ($totalDonations == 0 ? 0 : round(($served * 100 / $totalDonations), 0)) ?> % Served</span>
                        <div class="progress-bar-green">
                            <span class="progress-bar-filler" style="width: <?php // echo ($totalDonations == 0 ? 0 : round(($served * 100 / $totalDonations), 0)) 
                                                                            ?>%"></span>
                        </div>
                    </div>
                    <div class="btn1">
                       
                        <a href="<?php // echo Yii::app()->createUrl('donor/fundCampaign', array('token' => $campaign->token)) 
                                    ?>" class="pagetitle__button">Fund Campaign</a>
                        <?php // } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Campaign details -->
<div class="container camp-det">
    <div class="col-md-13">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#overview"><span class='glyphicon glyphicon-eye-open'></span> Overview</a>
            </li>
            <!-- This only displays the image button if the campaign has images -->
            <?php if(isset($images))if (count($images) !== 0) : ?>
                <li>
                    <a href="#images" style="color: #555"><span class='glyphicon glyphicon glyphicon-camera' ></span> Images</a>
                </li>
            <?php endif; ?>
            <!--End of image check-->

            <!-- This only displays the video button if the campaign has videos -->
            <?php if(isset($videos))if (count($videos) !== 0) : ?>
                <li>
                    <a href="#videos" style="color: #555"> <span class='glyphicon glyphicon-film'></span> Videos</a>
                </li>
            <?php endif; ?>
            <!--End of video check-->

            <!-- This only displays the recent campaign button if the campaign has donation -->
            <?php if(isset($fundCampaigns))if (count($fundCampaigns) !== 0) : ?>
                <li>
                    <a href="#donation"> <span class='glyphicon glyphicon-briefcase'></span> Recent Donations</a>
                </li>
            <?php endif; ?>

        </ul>
    </div>

    <div class="tab-content">
        <div id="overview" class="tab-pane fade in active">
            <div class="col-sm-4 col-xs-12">
                <div class="data-card-list__list">
                    <span class="data-card-list__icon">
                        <img style="" src="<?= Yii::getAlias('@web') ?>/assets/image/gfs-icons-campaign.png">
                    </span>
                    <span class="data-card-list__title"><?php echo $donors ?></span>
                    <span class="data-card-list__info">Numbers of Donors </span>
                </div>
            </div>

            <div class="col-sm-4 col-xs-12">
                <div class="data-card-list__list">
                    <span class="data-card-list__icon">
                        <img style="" src="<?= Yii::getAlias('@web') ?>/assets/image/gfs-icons-stamp.png">
                    </span>
                    <span class="data-card-list__title"><?php echo $totalDonations ?></span>

                    <span class="data-card-list__info">Total Donation </span>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="data-card-list__list">
                    <span class="data-card-list__icon">
                        <img style="" src="<?= Yii::getAlias('@web') ?>/assets/image/gfs-icons-meal.png">
                    </span>
                    <span class="data-card-list__title"><?php echo $served ?></span>
                    <span class="data-card-list__info">Served
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Images Section -->
<?php if (count($images) !== 0) : ?>
            <div class="container" id="images">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="recent-dtn" style="height: 300px;">
                            <h1 > Images</h1>
                            <div class="container">
                                <div class="row" style="margin-top: 10px;">
                                    <?php
                                    $n = 0;

                                    foreach ($images as $image) {
                                        $n++;
                                        if ($n != 4) { ?>
                                            <!-- Image display starts here -->
                                            <div class="col-sm-3">
                                                <img src="<?=  Yii::getAlias('@web') . '/open-image/read-file?imgt=' . $image->token; ?>" style="border-radius: 5px;" height="220" />
                                            </div>
                                            <!-- Image display ends here -->
                                        <?php } else { ?>
                                            <div class="col-sm-3">
                                                <div class="">
                                                    <img src="<?= Yii::getAlias('@web')  . '/open-image/read-file?imgt=' . $image->token; ?>" style="border-radius: 5px; width:70%;" height="150px">
                                                    <div class="caption">
                                                        <a class="btn btn-warning"
                                                        style="display:block; line-height: 50px; width:70%; text-align:center; margin-left: -1px; margin-top: 5px;"
                                                        href="<?= Yii::$app->urlManager->createUrl('site/view-campaign-image?token='.$campaign->token) ?>">View More</a>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- End of image Section -->

         <!-- Videos -->

         <?php //if (count($videos) !== 0) : ?>

        <div class="container" id="videos">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="recent-dtn" style="height: 300px;">
                        <h1> Videos</h1>
                        <div class="container">
                            <div class="row" style="margin-top: 10px;">
                                <?php
                                $v = 0;

                                foreach ($videos as $video) {
                                    $v++;
                                    if ($v != 4) { ?>
                                        <!-- Video display starts here -->
                                        <div class="col-sm-3">
                                            <div class="embed-responsive embed-responsive-4by3">
                                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $video['video_url'] ?>" style=" border-radius: 5px;" height="50px"></iframe>
                                            </div>
                                        </div>
                                        <!-- Video display ends here -->
                                    <?php } else { ?>
                                        <div class="col-sm-3">
                                            <div class="">

                                                <div class="embed-responsive embed-responsive-4by3">
                                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $video['video_url'] ?>" style="border-radius: 5px; width: 70%; height: 150px"></iframe>
                                                </div>
                                                <div class="caption">                                                                                                                              
                                                    <a class="btn btn-warning" style="display:block; line-height: 50px; width:70%; text-align:center; margin-left: -1px; margin-top: -38px;" href="<?= Yii::$app->urlManager->createUrl('site/view-campaign-video?token='.$campaign->token) ?>">View More</a>
                                                </div>
                                            </div>
                                        </div>
                                <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php //endif; ?>
        </div>
        </div>

        <!-- recent donations -->
        <div class="container" id="donation">
        <div class="row" >
        <div class="col-sm-12 col-xs-12">
        <div class="recent-dtn">
            <h1> Recent Donation</h1>
            <ul>
                <?php foreach ($recentDonations as $fundCampaign) { ?>
                    <li>
                        <div class="feed">
                            <img style="" src="<?php echo Yii::getAlias('@web') ?> . 'assets/image/man-user.png' ">
                            <div class="donr-det">
                                <span class="number"><?= $fundCampaign->foodstamp_volume ?> Food stamps<br />
                                    <?= isset($fundCampaign->donor)? $fundCampaign->donor->fullName() : "NADA"?>

                                </span>
                                <p class="time"><?php  echo Yii::$app->formatter->asRelativeTime($fundCampaign->create_time); ?> </p>
                            </div>
                        </div>
                        <div class="comment"><span>Comment:</span><?=  $fundCampaign->comment ?> </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php

        // $this->widget('CLinkPager', array(
        //     'pages' => $fcDataProvider->pagination ,
        // ));
        ?>

        <?= LinkPager::widget([ 'pagination' => $provider->pagination ]);?>

        
        </div>
        </div>
        </div>


        </div>
        <style>
        .pp_gallery li {
        display: none;
        }
        </style>
<script src="<?php echo Yii::$app->urlManager->createAbsoluteUrl('/') ?> /web/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" charset="utf-8">

console.log($(".pagination").find("a"));

var paginationLinksArr =  $(".pagination").find("a");

$.each(paginationLinksArr, function( index, value ) {
    value.href +="#donation";
 // console.log(value.href);
});
    
</script>
