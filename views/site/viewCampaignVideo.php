<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

?>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="pagetitle">
        <span class="pagetitle__title">
          <a href="<?= Yii::$app->urlManager->createUrl('dashboard') ?>">Campaign </a><span class="camp-title"> /<?php echo $campaign->title ?> </span>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="campaign-details">
        <div class="col-md-4 campaign-details__right-holder">
          <div class="campaign-details__right-holder">
            <span class="campaign-details__text">
            </span>
          </div>
        </div>
        <h2> <?php echo $campaign->title ?></h2>
        <div class="col-md-6">
          <div class="campaign-details__main">
            <p class=""><span>Created On: </span><?= date('F d, Y', strtotime($campaign->created_time)) ?></p>
            <p class=""><span>Description: </span><?= $campaign->description ?></p>
          </div>
        </div>
      </div>
    </div>
    <!-- Campaign details -->

    <div class="container">
      <div class="row">
        <div class="recent-dtn col-md-12" style="margin-bottom: 10px;">
          <h1> Videos</h1>
          <div class="row" style="overflow-y: scroll; height:400px;">
            <?php foreach ($videoGallery->getModels() as $video) { ?>
              <div class="col-md-3" style="margin-bottom: 3px;">
                <div class="embed-responsive embed-responsive-4by3">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $video->video_url; ?>" style=" border-radius: 5px;"></iframe>
                </div>
              </div>

            <?php } ?>
          </div>

        </div>
        <div class="container">
        <div class="row">
          <div class="col-md-12">
           <?= LinkPager::widget(['pagination' => $pagination,]); ?>
          </div>

          </div>
        </div>

        <!---End of row-->

      </div>
      <!---End of Container-->






      <!-- The Modal -->
      <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
      </div>
    </div>
  </div>




</div>

<script>
  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  var img = document.getElementById("myImg");
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  /* img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  } */


  $(".myImg").on('click', function() {
    modal.style.display = "block";
    modalImg.src = this.src;
    console.log(this.src);
    captionText.innerHTML = this.alt;
  });


  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }
</script>