<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\ContactForm;

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>



<style>
    .contact h3{
        font-size: 18px;
        color: #2e5f09;
    }
    .side-content{
        width: 70%;
        float: right;
    }
    .address {
        margin-top: 20px;
    }
</style>
<div class="container">
    <div class="wrapper">
        <div class="row row-offcanvas row-offcanvas-left gfs-inner">
            <?php if (Yii::$app->session->hasFlash('success')): ?>

              <div class="flash-success alert alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <h4><i class="icon fa fa-check"></i>Recieved!</h4>
                  <?= Yii::$app->session->getFlash('success'); ?>
              </div>

            <?php endif; ?>

            <!-- main right col -->
            <div class="column col-xs-12 contact" id="main">
                <div class="col-md-7">
                    <h3>Contact GiveFoodStamps </h3>
                    <div class="smart-wrap">
                        <div class="smart-forms  center-grid">

                            <?php $form = ActiveForm::begin(); ?>
                            <div class="frm-row">
                                <div class="section">
                                    <label class="field select" style="margin-bottom: 10px">
                                        <select id="account" name="inquiryType">
                                            <option value="">Type of Inquiry</option>
                                            <option value="donor"> Donation And Corporate Partnership</option>                                               
                                            <option value="partner">Technical Problem</option>
                                            <option value="partner">General Information And other inquiries</option>
                                        </select>
                                        <i class="arrow double"></i> 
                                    </label> 
                                </div>
                                <div class="section">
                                    <?= $form->field($contact, 'name')->label('Full Name')->textInput(['maxlength' => true, 'required' => 'required', 'placeholder' => 'e.g Gbenga Mohamed Ejike', 'class' => 'gui-input']) ?>
                                </div>

                                <div class="section">
                                    <?= $form->field($contact, 'email')->input('email', ['maxlength' => true, 'required' => 'required', 'placeholder' => 'Enter Email Here', 'class' => 'gui-input', 'style' => 'margin: 0px !important']) ?>
                                </div> 
                                <div class="section">
                                    <?= $form->field($contact, 'subject')->textInput(['maxlength' => true, 'required' => 'required', 'placeholder' => 'Enter Subject Here', 'class' => 'gui-input', 'style' => 'margin-top: 0px !important;']) ?>
                                </div>
                                <div class="section">
                                    <?= $form->field($contact, 'body')->textarea(['rows' => 2, 'cols' => 5, 'class' => 'gui-input'])->label('Enter Your Message'); ?>
                                </div>
                                <?= Html::submitButton('Submit', ['class' => 'button btn-primary btn-main']) ?>
                            </div>
                            <?php $form = ActiveForm::end(); ?>
                        </div><!-- end .smart-forms section -->
                    </div><!-- end .smart-wrap section -->
                </div>
                <div class="col-md-5">
                    <div class="side-content">
                        <a href="<?php echo Yii::$app->urlManager->createUrl('donate-now') ?>"> <img data-u="image" src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/contact.jpg" /></a>
                        <div class="address" >
                            <strong>Address</strong>
                            <p> 15 , Owukori Crescent , Surulere,<br/>
                                Alaka Estate , Lagos</p>


                            <strong> Tel:</strong>
                            <br/>
                            +234 908 246 3972 <br/> 
                            +234 908 246 3969
                        </div>
                    </div>

                </div>
            </div>
            <!-- /main -->
        </div>
    </div>
</div>