
    <div class="container">
        <div class="wrapper">
            <div class="row row-offcanvas row-offcanvas-left gfs-inner">
        <!-- sidebar -->
        <div class="column col-sm-3 col-xs-1 sidebar-offcanvas" id="sidebar">
            <ul class="nav" id="menu">
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>" ><span class="collapse in hidden-xs">About</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>" > <span class="collapse in hidden-xs">How it Works</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/stakeholders') ?>" class="active"> <span class="collapse in hidden-xs">Stakeholders</span></a></li>
                <li><a href="<?= Yii::$app->urlManager->createAbsoluteUrl('site/board') ?>"><span class="collapse in hidden-xs">Board</span></a></li>
         
           </ul>
        </div>
        <!-- /sidebar -->

        <!-- main right col -->
        <div class="column col-sm-9 col-xs-12" id="main">
            <a href="#" data-toggle="offcanvas"><i class="fa fa-navicon fa-2x"></i></a>
           <h1 class="big-title" id="about" >Stakeholders</h1>
            <hr/>
            <article class="regular-text-left">
             
            <h4 class="regular-text-left"><b>Donors:</b></h4>
            <p class="regular-text-left">The Food Stamps are paid for by the Donors. Donors comprises of Government, Multinational Donor Agencies, Corporate Organization and benevolent individual. Donors can purchase food stamps on this website and choose an NGO to distribute the stamps to the beneficiaries or can decide to distribute the food stamps themselves.. <a style="color: green" href="<?php //echo $this->createUrl('donor/login') ?>">Become Donor</a>
                
                
                </p>
                
                
                
            <h4 class="regular-text-left"><b>NGO (Partners) :</b></h4>
            <p class="regular-text-left">Non-Governmental Organisations are validated for their appropriate registrations and address verification before they are published on the website. Only after the validation and verification exercise shall the NGO be allowed to publish their work with the communities for which they solicit the feeding program.. <a style="color: green" href="<?php //echo $this->createUrl('partner/login') ?>">Become Partner</a></p>
            <h4 class="regular-text-left"><b>FOOD VENDOR NETWORK :</b></h4>
            <p class="regular-text-left">For enquiries on joining the GFS food network or bringing the food network to your city, please email info@givefoodstamps.com</p>
            <h4 class="regular-text-left"><b>BENEFICIARY :</b></h4>
            <p class="regular-text-left">The beneficiaries are indigent and less privileged in the society living below $1 a day</p>
            </article>
        </div>
        <!-- /main -->
    </div>
</div>
    </div>
    