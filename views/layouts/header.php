<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    
    <link type="text/css"  href="<?php echo Yii::getAlias('@app');  ?>/web/css/bootstrap.min.css" rel="stylesheet">

    <link type="text/css"  href="<?php echo Yii::getAlias('@app'); ?>/web/css/font-awesome.min.css" rel="stylesheet">
    <link type="text/css"  href="<?php echo Yii::getAlias('@app'); ?>/web/css/woome-interior-main.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::getAlias('@app'); ?>/web/css/slicknav.css" />


    <link rel="stylesheet" href="<?php echo Yii::getAlias('@app'); ?>/web/css/custom.css">
    <link rel="stylesheet" href="<?php echo Yii::getAlias('@app'); ?>/css/smart-forms.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121364859-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121364859-1');
    </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140166833-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-140166833-1');
    </script>
    <?php $this->head() ?>
</head>