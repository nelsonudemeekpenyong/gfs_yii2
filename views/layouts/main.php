<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $Author: Ekpenyong Nelson */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Users;
use Yii\helpers\Url;
use Yii\web\Session;
use yii\web\Application;
use app\models\User;



AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title>Givefoodstamps</title>

    <link type="text/css" href="http://localhost/gfs/web/css/bootstrap.min.css" rel="stylesheet">

    <link type="text/css" href="http://localhost/gfs/web/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" href="http://localhost/gfs/web/css/woome-interior-main.css" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost/gfs/web/css/slicknav.css" />
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->


    <link rel="stylesheet" href="http://localhost/gfs/web/css/custom.css">
    <link rel="stylesheet" href="http://localhost/gfs/web/css/smart-forms.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121364859-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-121364859-1');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140166833-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-140166833-1');
    </script>
    <?php $this->head() ?>
</head>

<body>
    <div class="body-holder">

        <div class="header">
            <div class=" container ">
                <div class="row">

                    <a href="<?php echo Yii::$app->urlManager->createUrl('site/index') ?>" class="header__logo col-md-2">
                        <img style="width: 100%" src="http://localhost/gfs/web/assets/image/woome-logo-white.png">
                    </a>
                    <!-- Navigation Starts Here -->
                    <div class="navigation">
                        <ul id="menu">
                            
                             <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->user_type) == 'donor') { ?>
                                <li><a href="<?php echo Yii::$app->urlManager->createUrl('site/logout') ?>"><strong>Logout</strong></a></li>
                                <li>
                                    <div class="">
                                        <a href="<?php echo Yii::$app->urlManager->createUrl('donor/profile') ?>"><?=  Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname. " (" . Yii::$app->user->identity->user_type . ")";?> <i class="fa fa-caret"></i></a>
                                    </div>
                                </li>
                                <li><a href="<?php echo Yii::$app->urlManager->createUrl('dashboard')    ?>">Dashboard</a></li>
                            <?php } else { ?>
                           
                                <?php if (Yii::$app->user->isGuest) { ?>
                                    <li><a href="<?php echo Yii::$app->urlManager->createUrl('site/login') ?>">Login</a></li>
                                    <li><a href="<?php echo Yii::$app->urlManager->createUrl('site/register') ?>">Sign Up</a></li>
                                <?php } ?>
                            <?php } ?>
                            <a class="btn btn-warning active" href="<?= Yii::$app->urlManager->createUrl('donate-now') ?>" style="margin-top: -8px;">Donate Now</a>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl('site/food-vendor-map') ?>">Food Network</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>">How It works</a></li>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl('site/campaigns') ?>">All Campaigns</a></li>
                        </ul>
                    </div> <!--- End of Navigation Div --->
                </div>
            </div>
        </div>

        <?php echo $content; ?>

    </div>
    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <?php if (isset(Yii::$app->session['err'])) { ?>
                        <div class="alert alert-warning ">

                            <?php echo Yii::$app->session['err'] ?>
                        </div>
                    <?php } ?>
                    <?php if (isset(Yii::$app->session['msg'])) { ?>
                        <div class="alert alert-success">

                            <?php echo Yii::$app->session['msg'] ?>
                        </div>
                    <?php } ?>
                    <button data-dismiss="modal" class="btn btn-danger green">OK</button>
                </div>

            </div>
        </div>
    </div>

    <script>
        <?php if (isset(Yii::$app->session['msg']) || isset(Yii::$app->session['err'])) { ?>
            $('#myModal2').modal();
        <?php } ?>
    </script>

    <?php
    if (isset(Yii::$app->session['msg'])) {
        unset(Yii::$app->session['msg']);
    }
    if (isset(Yii::$app->session['err'])) {
        unset(Yii::$app->session['err']);
    }
    ?>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer__menu">
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/how-it-works') ?>#howitworks">How It works</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>#about">About</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/stakeholders') ?>#stakeholders">Stakeholders</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/board') ?>#board">Board</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>#management">Management</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/contact') ?>#contact">Contact Us</a>
                        <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl('site/about') ?>#partners">Partners</a>
                    </div>
                </div>
                <div class="col-md-12">
                </div>
                <div class="col-md-12 footer__copyright">
                    Copyright givefoodstamps &copy; <?php echo '2016 - ' . date('Y') ?>
                </div>
                <div class="col-md-12 footer__socials">
                    <a href="#"><i class="fa fa-facebook-square"></i> </a>
                    <a href="https://twitter.com/Givefoodstamps"><i class="fa fa-twitter"></i> </a>
                    <a href="https://www.instagram.com/givefoodstamps/"><i class="fa fa-instagram"></i> </a>
                    <a href="https://www.youtube.com/channel/UCvab4-jYIVFw6V43TbJqIww"><i class="fa fa-youtube"></i> </a>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js" integrity="sha256-lnH4vnCtlKU2LmD0ZW1dU7ohTTKrcKP50WA9fa350cE=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <script src="<?php echo Yii::getAlias('@app') ?>/web/js/jquery.slicknav.js"></script>
    <script src="//code.tidio.co/4nfafupvfwstsagb6ql4x8h6smd6ns7d.js"></script>
    <script>
        $(function() {
            $('#menu').slicknav({
                label: '',
                duration: 1000,
                easingOpen: "easeOutBounce", //available with jQuery UI
            });

        });
    </script>
    <script>
        $('document').ready(function() {
            $('#description').hide();
            $('#descriptions').hide();

            if ($('#overviewclick').data('clicked')) {
                $('#overview').hide();
                $('#overviews').hide();
                $('#description').show();
                $('#descriptions').show();
            }

            if ($('#descriptionclick').data('clicked')) {
                $('#overview').show();
                $('#overviews').show();
                $('#description').hide();
                $('#descriptions').hide();
            }
        });
    </script>

</body>

</html>