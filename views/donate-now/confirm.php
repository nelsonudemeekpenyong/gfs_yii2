
<!-- Fluuter wave Check out Modal -->
<!-- <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script> -->
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <h2 style="margin-bottom: 0px;">Confirm Donation</h2>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-sm-12 col-md-6 ">
            <div class="spacer"></div>
            <form class="form-block" method="post">
                <div class="well text-center">
                  <p>Please confirm the donation: 
                        <br>
                    <div>You are about to sponsor the feeding of <?= $donateNowModel->food_stamps; ?> less privileged, for the cost of <b> <?= $donateNowModel->value; ?> <?= $currencySettings->currency_code; ?> <b> </div>
                  </p>
                </div>
                <label>
                    <span>Number of Food Stamp</span>
                    <input disabled type="number" id="foodStampsQty" name="number_of_food_stamps" value="<?php echo $donateNowModel->food_stamps; ?>" min="1" class="form__input" placeholder="Enter Number of Food Stamp" required="required" style="color: black;">
                </label>
                <label>
                    <span>Value in 
                        <b>
                        <?php if ($currencySettings->id == 1){echo "₦";} elseif ($currencySettings->id == 5){echo "$";} elseif ($currencySettings->id == 6){echo "€";} elseif ($currencySettings->id == 7){echo "£";}  ?>
                        </b>
                    </span>
                    <input type="text" id="value" name="value" value="<?php echo $donateNowModel->value; ?>" readonly class="form__input form-control" required="required">
                </label>

                <label>
                     <span>Payment Currency</span>   
                    <input type="text" name="pmtCurrency" value="<?php echo $currencySettings->currency_code; ?>" readonly class="form__input form-control">
                </label>

                <label>
                    <span>Comments</span>
                    <textarea disabled type="text" name="comments" id="comments" class="form__input form-control" placeholder="Enter Comment here" > <?php echo $donateNowModel->message; ?></textarea>
                </label>               
                 <label class="text-right">
                    <div class="button-green inline button-large" id="submit" style="border-radius: 5px;"/>Feed Now</div>
                </label>
            </form>
        </div>

    </div>
</div>





<script type="text/javascript" src="<?php echo Yii::$app->params['config']['raveUrl'] ?>"></script>
                <script>
                            document.addEventListener("DOMContentLoaded", function (event) {
                                document.getElementById("submit").addEventListener("click", function (e) {
                                    //  var PBFKey = "FLWPUBK-0240c5d578a4aa09ae4204ce8069ab9d-X";
                                    var PBFKey = "<?php echo Yii::$app->params['config']['PBFKey']; ?>";

                                    getpaidSetup({
                                        PBFPubKey: PBFKey,
                                        customer_email: "<?php echo $user->email ?>",
                                        customer_firstname: "<?php echo $user->firstname ?>",
                                        customer_lastname: "<?php echo $user->lastname ?>",
                                        custom_description: "Pay Internet",
                                        custom_logo: "http://localhost/gfs_design/assets/image/woome-logo-white.png",
                                        custom_title: "Give Food Stamps Ltd.",
                                        amount: "<?php echo $donateNowModel->value; ?>",
                                        customer_phone: "<?php echo $user->lastname ?>",
                                        country: "<?php echo $currencySettings->country_code ?>",
                                        currency: "<?php echo $currencySettings->currency_code ?>",
                                        payment_method: "both",
                                        txref: "<?php echo $trnxModel->trxref; ?>",
                                        //integrity_hash: "<ADD YOUR INTEGRITY HASH HERE>",
                                        onclose: function () {},
                                        callback: function (response) {
                                            var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                                            console.log("This is the response returned after a charge", response);
                                            if (
                                                    response.tx.chargeResponseCode == "00" ||
                                                    response.tx.chargeResponseCode == "0"
                                                    ) {

                                                $.post("<?php echo yii\helpers\BaseUrl::toRoute('donateNow/PaymentComplete')//echo Yii::$app->urlManager()->createUrl('donateNow/PaymentComplete') ?>", {"flw_ref": flw_ref}, function (data) {
                                                    // alert(data);
                                                    if (data == "0") {
                                                        window.location.replace("<?php echo yii\helpers\BaseUrl::toRoute('donateNow/pmtSucces') ?>");
                                                    } else {
                                                        window.location.replace("<?php echo yii\helpers\BaseUrl::toRoute('donateNow/PaymentFailure') ?>");
                                                    }
                                                });

                                            } else {
                                                // redirect to a failure page.
                                            }
                                        }
                                    });
                                });
                            });



                </script>