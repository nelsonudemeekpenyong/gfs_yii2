
<section class="home-section " style="padding:20 0px;">


<div class="container ">
    <div class="row">
        <div class="col-md-6 col-sm-8 col-xs-12 campaign-success">
            <div class="complete-icon">
                <img src="<?php echo Yii::$app->request->baseUrl; ?>/assets/image/checked.png" width="50%">
            </div>
            <h1 class="">Payment was Successful</h1>
            <hr/>
            <p>On behalf of the people who will be fed through your donation, thank you.</p>

            <hr/>
            <div class="row">
                <div class="col-sm-9 col-xs-12 btn2">
                    <p><a href="<?php echo $this->createUrl('donor/dashboard') ?>" class="button-green button-large">Go to Dashboard</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</section>



