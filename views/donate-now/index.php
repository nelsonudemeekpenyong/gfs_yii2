<?php
/* @var $this yii\web\View */
?>
<!-- Fluuter wave Check out Modal -->
<!-- <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script> -->

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="pagetitle">
                <h2 style="margin-bottom: 0px;">One Time Donation</h2>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-sm-12 col-md-6 ">
            <div class="spacer"></div>
             <form class="form-block" method="POST">
             
                <div class="well text-center">
                    <p>Food Stamps, which are valued at <b>₦ 350.00</b> are vouchers that carry a monetary value which can be exchanged for food at participating vendor.</p>
                </div>
                <label>
                    <span>Number of Food Stamp</span>
                    <input type="number" id="foodStampsQty" name="number_of_food_stamps" min="1" class="form__input" placeholder="Enter Number of Food Stamp" required="required" style="color: black;">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken; ?>">
                </label>
                <label>
                     <span>Payment Currency</span>   
                        <select name="currency_settings_id" class="form-control">
                            <?php foreach ($currencySettings as $cs) { ?>
                                <option value="<?php echo $cs->id ?>"><?php echo $cs->currency_code ?></option>
                            <?php } ?>
                        </select>
                </label>
                <label>
                    <span>Comments</span>
                    <textarea type="text" name="comments" id="comments" class="form__input form-control" placeholder="Enter Comment here"></textarea>
                </label>
                <label class="text-right">
                    <input type="submit" class="button-green inline button-large" value="Next " style="border-radius: 5px;" />
            </form>
        </div>

    </div>
</div>


<!-- Jquery that automatically calcultaes the value of food stamps and adds it to value field -->
<script>
    $("[name = number_of_food_stamps]").on('input',function() {
        var calc = ($(this).val() * 350);
        $("[name = value]").val(calc).css({
            "color": "black",
            "font-weight": "bold"
        });
        //$("#donateNow").append(calc);
    });

</script>
