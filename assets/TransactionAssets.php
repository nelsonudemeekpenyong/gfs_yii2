<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii;
use app\models\Transaction;


/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TransactionAssets
{
    

    public static function initTransaction($userid,$donateNowModel,$cSId){

		$trnxModel = new Transaction();
	    $trnxModel->donor_id        = $userid;
	    $trnxModel->payment_method  = "both";
        $trnxModel->trxref     = uniqid(true);
		$trnxModel->start_time  = date('Y-m-d H:i:s');
        $trnxModel->is_active  = 0;
        $trnxModel->amount  = $donateNowModel->food_stamps;
        $trnxModel->volume = (string)$donateNowModel->value;
        $trnxModel->description = "TOPUP";
        $trnxModel->currency_settings_id = $cSId;

        $trnxModel->save();

        return $trnxModel;
    }

}
