<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use app\models\Campaign;
use app\models\Country;
use app\models\Contact;
use app\models\State;
use app\models\LoginForm;
use app\models\Lga;
use app\models\User;
use app\models\CampaignGallery;
use app\models\search\CampaignGallerySearch;
use app\models\search\VideoSearch;
use app\models\FundCampaign;
use app\models\Video;
use yii\data\ActiveDataProvider;
use app\controllers\CHttpException;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use yii\base\ErrorException;


/**
 * SiteController implements the CRUD actions for Users model.
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = "main";
        return $this->render('home');
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionContact() {
        $contact = new Contact();
        $session = Yii::$app->session;
        
        if($contact->load(Yii::$app->request->post())){
          
          if($contact->save(false)){
             $session->setFlash('success', 'Thank You For Contacting Us. Your Message has been received');
          }else{
            $session->setFlash('error','We\'re sorry, something went wrong');
          }
          
          return $this->redirect('contact');
        }
        
        return $this->render('contact', [
            'contact' => $contact
        ]);
    }
    
    public function actionCampaigns()
    {

        $CampaingSearchModel = new \app\models\search\CampaignSearch();

        $dataProvider =  $CampaingSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('campaigns', [
            'searchModel' =>  $CampaingSearchModel,
            'dataProvider' => $dataProvider,
            'pagination' => $dataProvider->getPagination(),
            'models' => $dataProvider->getModels()
        ]);
    }
    public function actionViewCampaign()
    {
        $token = Yii::$app->request->get('token');
        $campaign = Campaign::findOne(['token' => $token]);
        $totalDonations = FundCampaign::find()->totalCampaignDonation($campaign->id);
        $served = FundCampaign::find()->totalServedDonation($campaign->id);
        $donors = FundCampaign::find()->totalDonor($campaign->id);
        $images = CampaignGallery::findBySql('select * from campaign_gallery order by id desc limit 4')->all();
        $videos = Video::findBySql('select * from video order by id desc limit 4')->all();


        $provider = new ActiveDataProvider(['query' =>  FundCampaign::find()
                ->where(['campaign_id' => $campaign->id]), 'pagination' => ['pageSize' => 3], 'sort' => ['defaultOrder' => ['create_time' => SORT_DESC]]]);
        $recentDonations = $provider->getModels();

        return $this->render(
            'viewcampaign',
            [
                'campaign' => $campaign,
                'totalDonations' => $totalDonations,
                'donors' => $donors,
                'served' => $served,
                'images' => $images,
                'videos' => $videos,
                'recentDonations' => $recentDonations,
                'provider' => $provider,
            ]
        );
    }

     public function actionViewCampaignImage($token)
    {
        $campaign = Campaign::findOne(['token' => $token]);
        
        $model = new CampaignGallerySearch();
        $imageGallery = $model->search(Yii::$app->request->queryParams);

        return $this->render('viewCampaignImage', [
            'campaign' => $campaign,
            'model' => $model,
            'imageGallery' => $imageGallery,
            'pagination' => $imageGallery->pagination
        ]);
    }
    
    public function actionViewCampaignVideo($token){
      
        $campaign = Campaign::findOne(['token' => $token]);
        
        $model = new VideoSearch();
        $videoGallery = $model->search(Yii::$app->request->queryParams);
        
          return $this->render('viewCampaignVideo', [
            'campaign' => $campaign,
            'model' => $model,
            'videoGallery' => $videoGallery,
            'pagination' => $videoGallery->pagination
        ]);
    }

    /* Methods for links on the NavBar from how-it-works*/
    public function actionHowItWorks(){
        return $this->render('HowItWorks');
    }
            public function actionAbout()
            {
                return $this->render('about');
            }

            public function actionStakeholders(){
                return $this->render('stakeholders');
            }

            public function actionBoard(){
                return $this->render('board');
            }
    /* End of methods for links */


    public function actionFoodVendorMap()
    {

        $connection = Yii::$app->getDb();

        $countryLists = Country::findBySql("select c.*,(select count(*) from food_vendors fv where fv.country = c.name) cnt from country c where c.is_active = 1")->all();


        $stateLists = $connection->createCommand('select *,(select count(*) from food_vendors fv where fv.city_id = s.id) cnt from state s where s.country_id = :country')
            ->bindValue('country', Yii::$app->request->get('country'))->queryAll();


        $lgaLists = $connection->createCommand('select *, (select count(*) from food_vendors fv where fv.lga = lg.name) cnt from lga lg where lg.state_id =:state')
            ->bindValue('state', Yii::$app->request->get('state'))->queryAll();


        return $this->render('FoodVendorMap', array(
            'countryLists' => $countryLists,
            'stateLists' => $stateLists,
            'lga' => $lgaLists
        ));
    }

    public function actionFoodvendorjson()
    {
        $connection = Yii::$app->getDb();
        $country = Yii::$app->request->get('country');
        $state = Yii::$app->request->get('state');
        $lga = Yii::$app->request->get('lga');

        $c = Country::findByAttributes(['id' => $country]);

        $l = Lga::model()->findByAttributes(['id' => $lga]);

        $ar = $connection->createCommand('select fv.first_name,fv.sur_name,fv.business_name,fv.addr_line_i,fv.addr_line_ii,fv.landmark,fv.lga,fv.country,fv.x_coordinate,fv.y_coordinate from food_vendors fv where fv.city_id = :state and fv.lga = :lga and fv.country = :country')
            ->bindValue('country', $c->name)
            ->bindValue('state', $state)
            ->bindValue('lga', $l->name)
            ->queryAll();

        header('Content-type: application/json');
        echo CJavaScript::jsonEncode($ar);
    }



     /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new Users();
        $session = Yii::$app->session;

        if ($model->load(Yii::$app->request->post())) {

            if($model->password != filter_input(INPUT_POST, 'confirmPassword')){

                $session->set('pwdError', 'Confirm password differs from password');
                $pwdError = $session;
                $this->redirect('register');
            }

            $model->token = uniqid();
            $model->password = md5($model->password);
            $model->save(false);
            $this->redirect(Yii::$app->urlManager->createUrl('dashboard'));
        }
        return $this->render('register', ['model'   => $model, /*'pwdError' => $pwdError*/ ]);
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())){
            if($model->login()) {
                return $this->goBack();
            }
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

/* ------------------------------------------------------------------------------------- */    
    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionStateList($id){
        
        $countOfStates =  State::find()->where(['id' => $id])->count();
        $states =  State::find()->where(['id'=> $id])->all();
        
        if($countOfStates > 0){
            foreach($states  as $state ){
                echo "<option value= ' " . $state->id. " ' >" . $state->name. "</option>";
            }
        }else{
            echo "<option></option>";
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


  

}
