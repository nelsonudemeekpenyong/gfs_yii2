<?php

namespace app\controllers;

use yii;
use yii\web\User;
use app\models\Campaign;
use app\models\search\FundCampaignSearch;
use app\models\search\CampaignSearch;
use app\models\FundCampaign;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class DashboardController extends \yii\web\Controller
{

    public function behaviors()
{
    return [
        'access' => [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['list'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ], // rules
        ], // access
    ];
}
    public function actionIndex()
    {

        $connection = Yii::$app->getDb();
        $userId = Yii::$app->user->identity->id;
         echo '<pre>'; var_dump($userId); echo '</pre>'; exit;
        $campaigns    = new Campaign();
        $fundCampaign = new FundCampaign();

        $my_activecampaignCount = $campaigns::find()->where(['is_active'=> 1, 'created_by'=> $userId])->count();

        $myactivecampaigns = $connection->createCommand( " select c.title,c.id,c.token,c.is_active,c.description,l.name state_name,c.created_time, (select sum(fc.foodstamp_volume) from fund_campaign fc where fc.campaign_id=c.id) volume, (select count(*) from voucher_batch_pins v where v.is_used=1 and v.campaign_id=c.id) used from campaign c,lga l where c.is_active=1 and l.id=c.location and c.created_by= $userId order by c.id desc")->queryAll(); 

        $mydonations  = $connection->createCommand("select ifnull(sum(t.volume),0) from transaction t where t.donor_id= $userId and !isnull(t.complete_time) and description='TOPUP'")->queryScalar();

        $mybalance    = $connection->createCommand("select sum( IF(description='TOPUP',t.volume,0) )+ sum( IF(description='DEBIT',t.volume,0) ) from transaction t where t.donor_id= $userId and !isnull(t.complete_time)")->queryScalar();

        $mealserved   = $connection->createCommand("select count(*) from campaign c, voucher_batch_pins v where v.campaign_id=c.id and c.created_by= $userId and v.is_activated=1 and v.is_used=1 and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1)=1 and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1)=1")->queryScalar();
       
        $mealunserved = $connection->createCommand("select count(*) from campaign c, voucher_batch_pins v where v.campaign_id=c.id and c.created_by= $userId and v.is_activated=1 and isnull(v.is_used) and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1) = 1 ")->queryScalar();

        $myCampaign2   = $campaigns::find()->where(['created_by'=> $userId ]);

        $myCampaignCount   = $myCampaign2->count();
        
        $model = new CampaignSearch();
        $data = $model->search(Yii::$app->request->queryParams);
        

        $this->layout = "main";
        

         return $this->render('index', [
            'mealunserved' => $mealunserved,
            'my_activecampaignCount' => $my_activecampaignCount,
            'myactivecampaigns' => $myactivecampaigns,
            'mydonations' => $mydonations,
            'mybalance' => $mybalance,
            'mealserved' => $mealserved,
            'user' => $userId,
            'data' => $data,
            'pagination' => $data->pagination
        ]);
    }
    
    public function actionMyFundedCampaigns(){
        $connection = Yii::$app->getDb();
        $fundCampaignSearch = new FundCampaignSearch();
        
        $model = $fundCampaignSearch->search(Yii::$app->request->queryParams);
        $user = Yii::$pp->user->identity->id;
        $my_activecampaignCount = $campaigns::find()->where(['is_active'=> 1, 'created_by'=> $userId])->count();

      
        $myFundedCampaigns = $connection->createCommand("select c.title,c.id,c.token,c.is_active,c.description,s.name state_name,c.created_time,
                sum(fc.foodstamp_volume) volume,fc.batch_token,
                (select count(*) from voucher_batch_pins v where v.is_used = 1 and v.campaign_id = c.id and v.donor_id = $user) used from campaign c, lga s, fund_campaign fc, users u
                where c.is_active = 1 and fc.campaign_id = c.id and !ISNULL(fc.batch_token) 
                and fc.created_by =  $user and s.id = c.location and u.id = c.channel_partner_id group by c.id order by fc.id desc")->queryAll();

        $mydonations = $connection->createCommand("select ifnull(sum(t.volume),0) from transaction t where t.donor_id=  $users and !isnull(t.complete_time) and description='TOPUP'")->queryScalar();
        $mealserved   = $connection->createCommand("select count(*) from campaign c, voucher_batch_pins v where v.campaign_id=c.id and c.created_by= $userId and v.is_activated=1 and v.is_used=1 and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1)=1 and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1)=1")->queryScalar();
        $mealunserved = $connection->createCommand("select count(*) from campaign c, voucher_batch_pins v where v.campaign_id=c.id and c.created_by= $userId and v.is_activated=1 and isnull(v.is_used) and (select count(*) from fund_campaign fc where fc.batch_token=v.batch_id limit 1) = 1 ")->queryScalar();


        $fcm = new FundCampaign('search');
        //$model->unsetAttributes();  // clear any default values
        

      

      return $this->render('my-funded-campaigns', array( 
          'myFundedCampaigns'=>$myFundedCampaigns, 
          'myFundedCampaignsCount'=>$myFundedCampaignsCount, 
          'mydonations'=>$mydonations,
           'mealserved'=>$mealserved, 
           'mealunserved'=>$mealunserved,
           
        ));
    }


}
