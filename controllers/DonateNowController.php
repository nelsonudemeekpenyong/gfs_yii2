<?php

namespace app\controllers;

use app\assets\TransactionAssets;
use Yii;
use \yii\web\Controller;
use app\models\DonateNow;
use app\models\CurrencySettings;
use app\models\Users;
use app\models\Transaction;


class DonateNowController extends Controller
{
    public function actionIndex()
    {
        $donateNowModel = new DonateNow();
                
       if(\Yii::$app->request->isPost) 
       {
            $no_of_food_stamps = filter_input(INPUT_POST, 'number_of_food_stamps');
            $currency_settings_id = filter_input(INPUT_POST, 'currency_settings_id');
            
            $currencySettings = CurrencySettings::find()->where(['id' => $currency_settings_id])->one()->stamp_value;
            
            $food_stamp_value = ($no_of_food_stamps *  $currencySettings);
            
            
            $comments = filter_input(INPUT_POST, 'comments');

            if(isset($no_of_food_stamps) && isset($currency_settings_id ))
            {
                $donateNowModel->food_stamps          = $no_of_food_stamps;
                $donateNowModel->value                = $food_stamp_value;
                $donateNowModel->currency_settings_id = $currency_settings_id;
				$donateNowModel->message            = $comments; 
				

				//This grabs all the data coming from the user that has been assigned to the db table and columns and puts it in a session
				Yii::$app->session['donateNowModel'] = $donateNowModel;

                 return $this->redirect(['confirm']);
            }else{
                Yii::$app->session['msg'] = "Invalid Input. Please restart the process";
            }
           
       }
       
        $currencySettings = CurrencySettings::find()->all();
               


        return $this->render('index',[
            'donateNowModel' => $donateNowModel,
            'currencySettings' =>  $currencySettings
        ]);
    }

	public function actionConfirm()
    {
        $donateNowModel = Yii::$app->session['donateNowModel'];
        if (isset($donateNowModel)) {
            $user = Users::find()->where(['email' => 'anonymous@givefoodstamps.com'])->one();

            if (isset($user)) {
                if (isset($user)) {
					$userId = (string) $user->id;
                }
				$currencySettings = CurrencySettings::find()->where(['id' => $donateNowModel->currency_settings_id])->one();

                if (isset($currencySettings)) {
					$trnxModel = TransactionAssets::initTransaction($userId, $donateNowModel, $currencySettings->id);
					       
                    $donateNowModel->trxref = $trnxModel->trxref;
                    $donateNowModel = Yii::$app->session['donateNowModel'];

                    return $this->render(
                            'confirm',
                            [
                                'trnxModel' => $trnxModel,
                                'donateNowModel' => $donateNowModel,
                                'currencySettings' => $currencySettings,
                                'user' => $user
                            ]
                        );
                } else {
                }
            } else {
            }
        } else {
        }

        Yii::$app->session['err'] = "An Issue has been detected. Please restart the Process";
        unset(Yii::$app->session['donateNowModel']);
        return $this->redirect(['index']);
    }


	public function actionPaymentComplete()
	{
		$donateNowModel = Yii::$app->session['donateNowModel'];
		unset(Yii::$app->session['donateNowModel']);
		if (isset($donateNowModel)) {
			$flw_ref = filter_input(INPUT_POST, 'flw_ref');

			if (isset($donateNowModel->trxref) && isset($flw_ref)) {


				$t = Transaction::find()->where(['trxref' => $donateNowModel->trxref])->one();
				if (isset($t)) {
					$t->is_active = 1;
					$t->complete_time = date('Y-m-d H:i:s');

					if ($t->save()) {
						Yii::$app->session['msg'] = "Payment was successful, you can now fund campaign with food stamps";
						isset(Yii::$app->user->route) ? Yii::$app->user->route = 0 : '';
						echo "0";
						return;
					}
				}
			}
			echo "1";
			return;
		}
	}


	public function actionPaymentSuccess()
	{
		if (isset(Yii::$app->session['donateNowModel'])) {
			return $this->render('pmtSuc');
		} else {
			Yii::$app->session['err'] = "Please resubmit form";
			unset(Yii::$app->session['donateNowModel']);
			return $this->redirect(['index']);
		}
	}

	public function actionPaymentFailure()
	{

		if (isset(Yii::$app->session['donateNowModel'])) {
			return $this->render('pmtFail');
		} else {
			Yii::$app->session['err'] = "Please resubmit form";
			unset(Yii::$app->session['donateNowModel']);
			return $this->redirect(['index']);
		}
	}



}
