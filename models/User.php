<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $firstname;
    public $lastname;
    public $user_type;



    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user =  User::getU(Users::findOne(['id' => $id]));
        return isset($user) ? new static($user) : null;
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user =  User::getU(
                    Users::findOne(['token' => $token])
        );

        return isset($user) ? new static($user) : null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($email)
    {
        $user = User::getU(Users::findOne(['email' => $email]));

        return isset($user) ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public static function getU($user)
    {

        if (isset($user)) {
            $usert = new User();
            $usert->username = $user->email;
            $usert->password = $user->password;
            $usert->id = $user->id;
            $usert->user_type = $user->user_type;
            $usert->accessToken = $user->token;
            $usert->firstname = $user->firstname;
            $usert->lastname = $user->lastname;
            return $usert;
        } else {
            return null;
        }
    }
}
