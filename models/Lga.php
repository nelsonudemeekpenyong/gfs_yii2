<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lga".
 *
 * @property int $id
 * @property int $stamp_cost
 * @property string $name
 * @property bool $is_active
 * @property int $state_id
 */
class Lga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stamp_cost', 'state_id'], 'integer'],
            [['name', 'state_id'], 'required'],
            [['is_active'], 'boolean'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stamp_cost' => 'Stamp Cost',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'state_id' => 'State ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\LgaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\LgaQuery(get_called_class());
    }
}
