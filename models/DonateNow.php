<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donate_now".
 *
 * @property int $id
 * @property int|null $anan_donations
 * @property int $food_stamps
 * @property string $value
 * @property string $message
 * @property int $currency_settings_id
 * @property string $trxref
 */
class DonateNow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'donate_now';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['anan_donations', 'food_stamps', 'currency_settings_id'], 'integer'],
            [['food_stamps', 'value', 'message', 'currency_settings_id', 'trxref'], 'required'],
            [['value', 'trxref'], 'string', 'max' => 200],
            [['message'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'anan_donations' => 'Anan Donations',
            'food_stamps' => 'Food Stamps',
            'value' => 'Value',
            'message' => 'Message',
            'currency_settings_id' => 'Currency Settings ID',
            'trxref' => 'Trxref',
        ];
    }

    /**
     * {@inheritdoc}
     * @return DonateNowQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DonateNowQuery(get_called_class());
    }
}
