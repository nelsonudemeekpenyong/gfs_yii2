<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property string $description
 * @property string $payment_method
 * @property string $trxref
 * @property string|null $flwref
 * @property string $donor_id
 * @property int|null $campaign_id
 * @property string $start_time
 * @property bool $is_active
 * @property float $amount
 * @property string|null $complete_time
 * @property int|null $batch_id
 * @property string $volume
 * @property int $currency_settings_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'payment_method', 'trxref', 'start_time', 'amount'], 'required'],
            [['description'], 'string'],
            [['campaign_id', 'batch_id', 'currency_settings_id'], 'integer'],
            [['start_time', 'complete_time'], 'safe'],
            [['is_active'], 'boolean'],
            [['amount'], 'number'],
            [['payment_method', 'donor_id', 'volume'], 'string', 'max' => 50],
            [['trxref', 'flwref'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'payment_method' => 'Payment Method',
            'trxref' => 'Trxref',
            'flwref' => 'Flwref',
            'donor_id' => 'Donor ID',
            'campaign_id' => 'Campaign ID',
            'start_time' => 'Start Time',
            'is_active' => 'Is Active',
            'amount' => 'Amount',
            'complete_time' => 'Complete Time',
            'batch_id' => 'Batch ID',
            'volume' => 'Volume',
            'currency_settings_id' => 'Currency Settings ID',
        ];
    }
}
