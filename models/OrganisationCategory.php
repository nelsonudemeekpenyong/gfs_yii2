<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_category".
 *
 * @property int $id
 * @property string $category
 * @property string $token
 * @property bool $is_active
 * @property string $created_time
 * @property int $created_by
 * @property int|null $updated_by
 * @property string|null $updated_time
 */
class OrganisationCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organisation_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'token', 'created_time', 'created_by'], 'required'],
            [['is_active'], 'boolean'],
            [['created_time', 'updated_time'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['category'], 'string', 'max' => 50],
            [['token'], 'string', 'max' => 30],
            [['category'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'token' => 'Token',
            'is_active' => 'Is Active',
            'created_time' => 'Created Time',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
        ];
    }
}
