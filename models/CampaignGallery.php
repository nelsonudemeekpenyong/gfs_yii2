<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campaign_gallery".
 *
 * @property int $id
 * @property int|null $campaign_id
 * @property string $file_name
 * @property string $token
 * @property string $media_type
 * @property int|null $created_by
 * @property int|null $channel_partner
 * @property string $created_time
 */
class CampaignGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campaign_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id', 'created_by', 'channel_partner'], 'integer'],
            [['file_name', 'token', 'media_type', 'created_time'], 'required'],
            [['created_time'], 'safe'],
            [['file_name'], 'string', 'max' => 500],
            [['token', 'media_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'file_name' => 'File Name',
            'token' => 'Token',
            'media_type' => 'Media Type',
            'created_by' => 'Created By',
            'channel_partner' => 'Channel Partner',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CampaignGalleryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CampaignGalleryQuery(get_called_class());
    }
}
