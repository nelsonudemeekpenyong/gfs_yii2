<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campaign".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $volume
 * @property int|null $channel_partner_id
 * @property int|null $donor_id
 * @property string $user_type
 * @property int $location
 * @property string|null $end_date
 * @property string|null $start_date
 * @property string|null $background_color
 * @property string|null $text_color
 * @property string|null $picture
 * @property string $token
 * @property int|null $is_active
 * @property int|null $is_featured
 * @property int $created_by
 * @property string $created_time
 * @property string|null $update_time
 * @property int|null $updated_by
 */
class Campaign extends \yii\db\ActiveRecord
{

    public $locationName;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'user_type', 'location', 'token', 'created_by', 'created_time'], 'required'],
            [['description'], 'string'],
            [['volume', 'channel_partner_id', 'donor_id', 'location', 'is_active', 'is_featured', 'created_by', 'updated_by'], 'integer'],
            [['end_date', 'start_date', 'created_time', 'update_time'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['user_type', 'background_color', 'text_color'], 'string', 'max' => 7],
            [['picture'], 'string', 'max' => 500],
            [['token'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'volume' => 'Volume',
            'channel_partner_id' => 'Channel Partner ID',
            'donor_id' => 'Donor ID',
            'user_type' => 'User Type',
            'location' => 'Location',
            'end_date' => 'End Date',
            'start_date' => 'Start Date',
            'background_color' => 'Background Color',
            'text_color' => 'Text Color',
            'picture' => 'Picture',
            'token' => 'Token',
            'is_active' => 'Is Active',
            'is_featured' => 'Is Featured',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CampaignQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CampaignQuery(get_called_class());
    }
}
