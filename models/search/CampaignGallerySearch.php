<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CampaignGallery;

/**
 * CampaignGallerySearch represents the model behind the search form of `app\models\CampaignGallery`.
 */
class CampaignGallerySearch extends CampaignGallery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'campaign_id', 'created_by', 'channel_partner'], 'integer'],
            [['file_name', 'token', 'media_type', 'created_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CampaignGallery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 6],
            'sort' => ['defaultOrder' => ['created_time' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'campaign_id' => $this->campaign_id,
            'created_by' => $this->created_by,
            'channel_partner' => $this->channel_partner,
            'created_time' => $this->created_time,
        ]);

        
        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'media_type', $this->media_type]);

        return $dataProvider;
    }
}
