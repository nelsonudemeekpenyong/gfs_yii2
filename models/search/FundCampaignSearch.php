<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FundCampaign;

/**
 * FundCampaignSearch represents the model behind the search form of `app\models\FundCampaign`.
 */
class FundCampaignSearch extends FundCampaign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'donor_id', 'campaign_id', 'batch_token', 'created_by', 'updated_by', 'is_active', 'foodstamp_rate', 'foodstamp_volume', 'print_partner_id', 'delivery_partner_id', 'is_sent'], 'integer'],
            [['user_type', 'token', 'create_time', 'updated_time', 'completed_time', 'voucher_creation_time', 'status', 'comment'], 'safe'],
            [['donation_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FundCampaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'donor_id' => $this->donor_id,
            'campaign_id' => $this->campaign_id,
            'batch_token' => $this->batch_token,
            'create_time' => $this->create_time,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
            'is_active' => $this->is_active,
            'foodstamp_rate' => $this->foodstamp_rate,
            'foodstamp_volume' => $this->foodstamp_volume,
            'donation_amount' => $this->donation_amount,
            'completed_time' => $this->completed_time,
            'voucher_creation_time' => $this->voucher_creation_time,
            'print_partner_id' => $this->print_partner_id,
            'delivery_partner_id' => $this->delivery_partner_id,
            'is_sent' => $this->is_sent,
        ]);

        $query->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
