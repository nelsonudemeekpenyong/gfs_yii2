<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Campaign;

/**
 * CamapignSearch represents the model behind the search form of `app\models\Campaign`.
 */
class CampaignSearch extends Campaign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'volume', 'channel_partner_id', 'donor_id', 'location', 'is_active', 'is_featured', 'created_by', 'updated_by'], 'integer'],
            [['title', 'description', 'user_type', 'end_date', 'start_date', 'background_color', 'text_color', 'picture', 'token', 'created_time', 'update_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 3],
            'sort' => [
                'defaultOrder' => 
                          [
                              'created_time' => SORT_DESC,
                          ]
                
                      ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'volume' => $this->volume,
            'channel_partner_id' => $this->channel_partner_id,
            'donor_id' => $this->donor_id,
            'location' => $this->location,
            'end_date' => $this->end_date,
            'start_date' => $this->start_date,
            'is_active' => $this->is_active,
            'is_featured' => $this->is_featured,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'background_color', $this->background_color])
            ->andFilterWhere(['like', 'text_color', $this->text_color])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'token', $this->token]);
        return $dataProvider;
    }
}
