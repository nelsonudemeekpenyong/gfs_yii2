<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $name
 * @property bool $is_active
 * @property string $token
 * @property string $created_time
 * @property string|null $updated_time
 * @property int $created_by
 * @property int|null $updated_by
 *
 * @property City[] $cities
 */
class Country extends \yii\db\ActiveRecord
{

    public $cnt;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'token', 'created_time', 'created_by'], 'required'],
            [['is_active'], 'boolean'],
            [['created_time', 'updated_time'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['token'], 'string', 'max' => 30],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'token' => 'Token',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Cities]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\CityQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CountryQuery(get_called_class());
    }
}
