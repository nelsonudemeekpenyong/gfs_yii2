<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "state".
 *
 * @property int $id
 * @property string|null $name
 * @property bool $is_active
 * @property int $country_id
 * @property int $stamp_cost
 */
class State extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_active'], 'boolean'],
            [['country_id', 'stamp_cost'], 'required'],
            [['country_id', 'stamp_cost'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'country_id' => 'Country ID',
            'stamp_cost' => 'Stamp Cost',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\StateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\StateQuery(get_called_class());
    }
}
