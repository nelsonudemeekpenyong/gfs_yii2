<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Users".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string|null $account_type
 * @property string $address
 * @property string $state
 * @property string $lga
 * @property string $token
 * @property string|null $last_login
 * @property string $user_type
 * @property string|null $organisation_name
 * @property int|null $organisation_category_id
 * @property string|null $description
 * @property string|null $operating_state
 * @property string|null $contact_number
 * @property int|null $parent_id
 * @property int|null $country_id
 * @property string|null $confirm_time
 * @property string $created_time
 * @property string|null $updated_time
 * @property int $is_active
 * @property int|null $is_approve
 * @property int|null $updated_by
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'password', 'address', 'state', 'token', 'user_type', 'created_time'], 'required'],
            [['last_login', 'confirm_time', 'created_time', 'updated_time'], 'safe'],
            [['organisation_category_id', 'parent_id', 'country_id', 'is_active', 'is_approve', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['firstname', 'lastname', 'email', 'organisation_name'], 'string', 'max' => 100],
            [['password', 'address'], 'string', 'max' => 200],
            [['account_type', 'state', 'lga'], 'string', 'max' => 50],
            [['token', 'operating_state'], 'string', 'max' => 30],
            [['user_type'], 'string', 'max' => 7],
            [['contact_number'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'password' => 'Password',
            'account_type' => 'Account Type',
            'address' => 'Address',
            'state' => 'State',
            'lga' => 'Lga',
            'token' => 'Token',
            'last_login' => 'Last Login',
            'user_type' => 'User Type',
            'organisation_name' => 'Organisation Name',
            'organisation_category_id' => 'Organisation Category ID',
            'description' => 'Description',
            'operating_state' => 'Operating State',
            'contact_number' => 'Contact Number',
            'parent_id' => 'Parent ID',
            'country_id' => 'Country ID',
            'confirm_time' => 'Confirm Time',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'is_active' => 'Is Active',
            'is_approve' => 'Is Approve',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\UsersQuery(get_called_class());
    }
}
