<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fund_campaign".
 *
 * @property int $id
 * @property int $donor_id
 * @property int $campaign_id
 * @property string $user_type
 * @property int|null $batch_token
 * @property string $token
 * @property string $create_time
 * @property int $created_by
 * @property int|null $updated_by
 * @property string|null $updated_time
 * @property int $is_active
 * @property int $foodstamp_rate
 * @property int $foodstamp_volume
 * @property float $donation_amount
 * @property string|null $completed_time
 * @property string|null $voucher_creation_time
 * @property string|null $status
 * @property int|null $print_partner_id
 * @property int|null $delivery_partner_id
 * @property int|null $is_sent
 * @property string|null $comment
 */
class FundCampaign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fund_campaign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['donor_id', 'campaign_id', 'user_type', 'token', 'create_time', 'created_by', 'foodstamp_rate', 'foodstamp_volume', 'donation_amount'], 'required'],
            [['donor_id', 'campaign_id', 'batch_token', 'created_by', 'updated_by', 'is_active', 'foodstamp_rate', 'foodstamp_volume', 'print_partner_id', 'delivery_partner_id', 'is_sent'], 'integer'],
            [['create_time', 'updated_time', 'completed_time', 'voucher_creation_time'], 'safe'],
            [['donation_amount'], 'number'],
            [['user_type'], 'string', 'max' => 7],
            [['token'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'donor_id' => 'Donor ID',
            'campaign_id' => 'Campaign ID',
            'user_type' => 'User Type',
            'batch_token' => 'Batch Token',
            'token' => 'Token',
            'create_time' => 'Create Time',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
            'is_active' => 'Is Active',
            'foodstamp_rate' => 'Foodstamp Rate',
            'foodstamp_volume' => 'Foodstamp Volume',
            'donation_amount' => 'Donation Amount',
            'completed_time' => 'Completed Time',
            'voucher_creation_time' => 'Voucher Creation Time',
            'status' => 'Status',
            'print_partner_id' => 'Print Partner ID',
            'delivery_partner_id' => 'Delivery Partner ID',
            'is_sent' => 'Is Sent',
            'comment' => 'Comment',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\FundCampaignQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\FundCampaignQuery(get_called_class());
    }
}
