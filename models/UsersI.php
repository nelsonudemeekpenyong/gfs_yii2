<?php

/**
 * This is the model class for table "users_i".
 *
 * The followings are the available columns in table 'users_i':
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $account_type
 * @property string $address
 * @property string $state
 * @property string $lga
 * @property string $token
 * @property string $last_login
 * @property string $user_type
 * @property string $organisation_name
 * @property integer $organisation_category_id
 * @property string $description
 * @property string $operating_state
 * @property string $contact_number
 * @property integer $parent_id
 * @property integer $country_id
 * @property string $confirm_time
 * @property string $created_time
 * @property string $updated_time
 * @property integer $is_active
 * @property integer $is_approve
 * @property integer $updated_by
 */
class UsersI extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_i';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, email, password, address, state, lga, token, user_type, created_time', 'required'),
			array('organisation_category_id, parent_id, country_id, is_active, is_approve, updated_by', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, email, organisation_name', 'length', 'max'=>100),
			array('password, address', 'length', 'max'=>200),
			array('account_type, state, lga', 'length', 'max'=>50),
			array('token, operating_state', 'length', 'max'=>30),
			array('user_type', 'length', 'max'=>7),
			array('contact_number', 'length', 'max'=>15),
			array('last_login, description, confirm_time, updated_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstname, lastname, email, password, account_type, address, state, lga, token, last_login, user_type, organisation_name, organisation_category_id, description, operating_state, contact_number, parent_id, country_id, confirm_time, created_time, updated_time, is_active, is_approve, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'email' => 'Email',
			'password' => 'Password',
			'account_type' => 'Account Type',
			'address' => 'Address',
			'state' => 'State',
			'lga' => 'Lga',
			'token' => 'Token',
			'last_login' => 'Last Login',
			'user_type' => 'User Type',
			'organisation_name' => 'Organisation Name',
			'organisation_category_id' => 'Organisation Category',
			'description' => 'Description',
			'operating_state' => 'Operating State',
			'contact_number' => 'Contact Number',
			'parent_id' => 'Parent',
			'country_id' => 'Country',
			'confirm_time' => 'Confirm Time',
			'created_time' => 'Created Time',
			'updated_time' => 'Updated Time',
			'is_active' => 'Is Active',
			'is_approve' => 'Is Approve',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('account_type',$this->account_type,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('lga',$this->lga,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('organisation_name',$this->organisation_name,true);
		$criteria->compare('organisation_category_id',$this->organisation_category_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('operating_state',$this->operating_state,true);
		$criteria->compare('contact_number',$this->contact_number,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('confirm_time',$this->confirm_time,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_approve',$this->is_approve);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersI the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
