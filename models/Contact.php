<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $body
 * @property string|null $subject
 * @property string $time
 */
class Contact extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'body', 'time'], 'required'],
            [['name'], 'string'],
            [['time'], 'safe'],
            [['email', 'subject'], 'string', 'max' => 50],
            [['body'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'body' => 'Body',
            'subject' => 'Subject',
            'time' => 'Time',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ContactQuery(get_called_class());
    }
}
