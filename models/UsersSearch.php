<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'organisation_category_id', 'parent_id', 'country_id', 'is_active', 'is_approve', 'updated_by'], 'integer'],
            [['firstname', 'lastname', 'email', 'password', 'account_type', 'address', 'state', 'lga', 'token', 'last_login', 'user_type', 'organisation_name', 'description', 'operating_state', 'contact_number', 'confirm_time', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'last_login' => $this->last_login,
            'organisation_category_id' => $this->organisation_category_id,
            'parent_id' => $this->parent_id,
            'country_id' => $this->country_id,
            'confirm_time' => $this->confirm_time,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            'is_active' => $this->is_active,
            'is_approve' => $this->is_approve,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'lga', $this->lga])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'organisation_name', $this->organisation_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'operating_state', $this->operating_state])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number]);

        return $dataProvider;
    }
}
