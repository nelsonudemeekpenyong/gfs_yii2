<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency_settings".
 *
 * @property int $id
 * @property string $currency_code
 * @property string|null $country_code
 * @property string|null $stamp_value
 * @property string $token
 * @property string $created_time
 * @property string|null $updated_time
 * @property int $created_by
 * @property int|null $updated_by
 */
class CurrencySettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_code', 'token', 'created_time', 'created_by'], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['currency_code', 'country_code', 'stamp_value'], 'string', 'max' => 50],
            [['token'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_code' => 'Currency Code',
            'country_code' => 'Country Code',
            'stamp_value' => 'Stamp Value',
            'token' => 'Token',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
