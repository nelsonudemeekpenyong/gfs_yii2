<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property int|null $campaign_id
 * @property string $token
 * @property string|null $video_url
 * @property int|null $created_by
 * @property int|null $channel_partner
 * @property string $created_time
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id', 'created_by', 'channel_partner'], 'integer'],
            [['token', 'created_time'], 'required'],
            [['created_time'], 'safe'],
            [['token', 'video_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'token' => 'Token',
            'video_url' => 'Video Url',
            'created_by' => 'Created By',
            'channel_partner' => 'Channel Partner',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\VideoQuery(get_called_class());
    }
}
