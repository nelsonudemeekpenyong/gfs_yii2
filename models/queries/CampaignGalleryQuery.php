<?php

namespace app\models\queries;

use Yii;

/**
 * This is the ActiveQuery class for [[\app\models\CampaignGallery]].
 *
 * @see \app\models\CampaignGallery
 */
class CampaignGalleryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\CampaignGallery[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\CampaignGallery|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    // public function allImages()
    // {
    //     $images = Yii::$app->db->createCommand('select * from campaign_gallery order by id desc limit 4')->queryScalar();
    //     return $images;
    // }
}
