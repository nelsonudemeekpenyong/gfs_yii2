<?php

namespace app\models\queries;

use Yii;

use Yii\db\ActiveQuery;
use Yii\db\Query;
use app\models\fundCampaign;

/**
 * This is the ActiveQuery class for [[\app\models\FundCampaign]].
 *
 * @see \app\models\FundCampaign
 */
class FundCampaignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\FundCampaign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\FundCampaign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function totalCampaignDonation($campaignId){
        $totalDonations = Yii::$app->db->createCommand("select sum(fc.foodstamp_volume) from fund_campaign fc
        where fc.campaign_id = :campaignId",[':campaignId'=>$campaignId])->queryScalar();
        return $totalDonations;
        }

    public function totalServedDonation($campaignId){
        $served = Yii::$app->db->createCommand("select count(*) from fund_campaign fc , voucher_batch_pins vbp 
        where vbp.batch_id = fc.batch_token and vbp.is_used =1 and fc.campaign_id =  :campaignId",[':campaignId'=>$campaignId])->queryScalar();
        return $served;
    }

    public function totalDonor($campaignId){
        $served = Yii::$app->db->createCommand("select count(*) from (select  count(*)  from fund_campaign fc 
        where fc.campaign_id = :campaignId
        group by fc.donor_id) n;",[':campaignId'=>$campaignId])->queryScalar();
        return $served;
    }

    public function recentDonationsToACampaign($campaignId){
        //$fundCampaigns = Yii::$app->db->createCommand(" select ")->queryScalar();

        //$donors = Yii::$app->db->createCommand("select * from fund_campaign fc where fc.campaign_id = :campaignId", [':campaignId' => $campaignId])->queryScalar();

        $query = FundCampaign::find()->where(['campaign_id' => $campaignId]);

        $provider = new \Yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ]
            ],
        ]);
        
        $provider->getModels();
        return $provider;
    }

}
