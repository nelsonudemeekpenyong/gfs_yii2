<?php

namespace app\models\queries;


use Yii;

/**
 * This is the ActiveQuery class for [[\app\models\Campaign]].
 *
 * @see \app\models\Campaign
 */
class CampaignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\Campaign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Campaign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }



}
