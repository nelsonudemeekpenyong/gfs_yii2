<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 * @property float $cost_of_food_stamp
 * @property int $country_id
 * @property string $token
 * @property bool $is_active
 * @property int $created_by
 * @property string $created_time
 * @property string|null $updated_time
 * @property int|null $updated_by
 *
 * @property Country $country
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'cost_of_food_stamp', 'country_id', 'token', 'created_by', 'created_time'], 'required'],
            [['cost_of_food_stamp'], 'number'],
            [['country_id', 'created_by', 'updated_by'], 'integer'],
            [['is_active'], 'boolean'],
            [['created_time', 'updated_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['token'], 'string', 'max' => 30],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cost_of_food_stamp' => 'Cost Of Food Stamp',
            'country_id' => 'Country ID',
            'token' => 'Token',
            'is_active' => 'Is Active',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery|\app\models\queries\CountryQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\CityQuery(get_called_class());
    }
}
